package com.sapfil.ds.Objects.PhysicsObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.HeroConstants;

/************************************************
 * Created by Sapfil on 27.12.2015.
 * Last edit: 16-10-2016
 * Notes: in "ACTIVE CODING" status
 ***********************************************/
public class Hero extends HealthObject {
    // ===========================================================
    // Constants
    // ===========================================================

    public enum Status {MOVINT_TO_TARGET, MOVING_TO_OBJECT, MOVING_TO_EMPTY_POINT, STAY_AND_AIM, STAY_AND_MINE, STAY}

    // ===========================================================
    // Fields
    // ===========================================================

    private float target_D = 0, target_A, target_X, target_Y;
    private Status mCurrentStatus = Status.STAY;
    //private IHealthObject mAimObject;
    private int BrakeDistance = HeroConstants.EMPTY_POINT_STOP_DISTANCE;
    private float crashTimer = -1;
    private HeroMiningGun mMiningGun = new HeroMiningGun();
    private IHealthObject pTarget = null;

    // ===========================================================
    // Constructors
    // ===========================================================

    public Hero() {  super(1); }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public void SetStatus (Status s){ mCurrentStatus = s; }

    public void SetMoveTargetObject (IHealthObject pTargetObject){
       mCurrentStatus = Status.MOVING_TO_OBJECT;
       if (pTargetObject.GetBody().getType() == Body.BodyType.SPHERE) {
           target_X = pTargetObject.GetBody().getX();
           target_Y = pTargetObject.GetBody().getY();
           BrakeDistance = HeroConstants.DRILL_MAX_DISTANCE + pTargetObject.GetBody().getWidthOrRadius() + this.GetBody().getWidthOrRadius();
       }
       pTarget = pTargetObject;
        mMiningGun.setAimPoint(pTargetObject.getCenterX(), pTargetObject.getCenterY());
   }

    public void SetMoveTargetPoint (float pTarget_X, float pTarget_Y){
        mCurrentStatus = Status.MOVING_TO_EMPTY_POINT;
        BrakeDistance = HeroConstants.EMPTY_POINT_STOP_DISTANCE;
        target_X = pTarget_X;
        target_Y = pTarget_Y;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public void render(SpriteBatch pSB){
        pSB.begin();
          super.render(pSB);
        pSB.end();

        mMiningGun.setParentParameters(this.getCenterX(), this.getCenterY(), this.getA());
        mMiningGun.render(pSB);
    }

    public byte update (float dt){
        byte super_return = super.update(dt);
        mMiningGun.update(dt);


        // TODO: Try to delete this
        //this.ResetBody();
        //Gdx.app.log("Sapfil_Log", "STATUS: " + this.mCurrentStatus );
        //mMiningGun.On();
        if (this.crashTimer < 0)


            switch (mCurrentStatus){
                case STAY: {
                    this.percentModifySpeed(HeroConstants.HERO_BRAKING, dt);
                    setSpeedA(0);
                    break;}

                case STAY_AND_MINE:{
                    this.Target_A_Calculation();
                    this.Target_D_Calculation();
                    mMiningGun.SetTargetParameters(this.getA(), target_D, target_A);
                    mMiningGun.On();
                    if (this.getSpeedV() > HeroConstants.MAX_STAY_SPEED / 2)
                        this.percentModifySpeed(0.5f * HeroConstants.HERO_BRAKING, dt);
                    else
                        this.setSpeedV(0);
                    pTarget.ReduceHealth(500 * dt);
                    if (pTarget.GetHealth() <= 0) {
                        //pTarget = null;
                        mCurrentStatus = Status.STAY;
                        mMiningGun.Off();
                    }
                    break;
                }

                case STAY_AND_AIM:{

                    this.Target_A_Calculation();
                    this.Turn_To_Target();
                    this.percentModifySpeed(0.5f * HeroConstants.HERO_BRAKING, dt);
                    if (this.getSpeedA() == 0)
                        mCurrentStatus = Status.STAY_AND_MINE;
                    break;
                }

                case MOVING_TO_OBJECT:

                case MOVING_TO_EMPTY_POINT: {

                    mMiningGun.Off();

                    this.Target_D_Calculation();
                    //Gdx.app.log("Sapfil_Log", "Target_D: " + target_D + " Target_X: " + target_X + " Target_Y: " + target_Y  + " Hero_X: " + this.mX + " Hero_Y: " + this.mY);

                    if (target_D > BrakeDistance){
                        this.Target_A_Calculation();
                        this.Turn_To_Target(true);
                        //if (MathUtils.cosDeg(this.mA + target_A) < -HeroConstants.AIM_ANGLE_COS)
                            this.setSpeedV(this.getSpeedV() + HeroConstants.HERO_ACCELERATION * dt);
                    }
                    else {
                        if (mCurrentStatus == Status.MOVING_TO_EMPTY_POINT)
                            mCurrentStatus = Status.STAY;
                        else
                            mCurrentStatus = Status.STAY_AND_AIM;
                    }
                }
            }
        else {
            this.crashTimer -= dt;
            // TODO: try to delete this
            mCurrentStatus = Status.STAY;
            this.percentModifySpeed(HeroConstants.HERO_BRAKING, dt);
            setSpeedA(0);
        }

        if (this.getSpeedV() > HeroConstants.HERO_MAX_SPEED)
            this.setSpeedV(HeroConstants.HERO_MAX_SPEED);

        return super_return;
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // freezes hero on little time to avoid JAM-error
    public void crashFreeze(float time){
        this.crashTimer = time;
    }

    public boolean isMoving(){
        switch (mCurrentStatus){
            case STAY:
            case STAY_AND_MINE:
            case STAY_AND_AIM: {return false;}
            case MOVING_TO_EMPTY_POINT:
            case MOVING_TO_OBJECT:
            case MOVINT_TO_TARGET:
            default: return true;
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================



    private void Target_A_Calculation(){
        target_A = - MathUtils.atan2(target_X - this.getCenterX(), target_Y - this.getCenterY())
                    * MathUtils.radiansToDegrees;
    }

    private void Target_D_Calculation(){
        target_D = (float) Math.hypot((double) (this.getCenterX() - target_X), (double) (this.getCenterY() - target_Y));
    }

    private void Turn_To_Target () {this.Turn_To_Target(false);}
    private void Turn_To_Target(boolean directAiming){
        if (Math.abs(MathUtils.sinDeg(this.getA() - target_A)) > HeroConstants.AIM_ANGLE_SIN_MAX_ERROR || MathUtils.cosDeg(this.getA() - target_A) < 0 )
        {
            if (MathUtils.sinDeg(this.getA() - target_A) > 0)
                this.setSpeedA(-HeroConstants.HERO_RADIAL_SPEED);
            else
                this.setSpeedA(HeroConstants.HERO_RADIAL_SPEED);
        }
        else {
            this.setSpeedA(0);
            if (directAiming){
                this.setA(target_A);            }
        }
        }
}
