package com.sapfil.ds.Objects.PhysicsObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.Objects.GFXObjects.BasicAnimatedObject;
import com.sapfil.ds.Objects.GFXObjects.BasicObject;
import com.sapfil.ds.Objects.GFXObjects.IBasicObject;
import com.sapfil.ds.XML.DataBaseFromXML;

import org.w3c.dom.Node;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 09-10-2016
 * Notes:
 ***********************************************/
public class HealthObject /*extends BasicAnimatedObject*/ implements IHealthObject {
    // ===========================================================
    // Constants
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    private float deltaX, deltaY; // body offset from sprite corner-coordinate
    protected float health;
    private Body mBody;
    public Node worldDBNode = null;
    IBasicObject sprite;
    private byte r;

    // ===========================================================
    // Constructors
    // ===========================================================

    // body_x and body_y *** 02-03-2013
    public HealthObject(int id){this(id,0,0);}
    public HealthObject(int id, float x, float y){this(id,x,y,0,0);}
    public HealthObject(int id, float x, float y, float vx, float vy){this(id, 1, x, y, vx, vy);}
    public HealthObject(int id, float scale, float x, float y, float vx, float vy)	{this(id, scale, 0, x, y, vx, vy, 0);}
    public HealthObject(int id, float scale, float angle,  float x, float y, float vx, float vy, float va){

        if (sDB.BasicPartMap.get(id).fps != 0)
            sprite = new BasicAnimatedObject(id,x,y,vx,vy);
        else
            sprite = new BasicObject(id,x,y,vx,vy);

        //if (angle != 0)
        //    this.setA(angle);

        if (sDB.HealthPartMap.get(id).body_par1 != 0 ){
            if (sDB.HealthPartMap.get(id).bodyType == 0) {      //bodyType == 0 - circle body
                this.mBody = new Body(x, y, sDB.HealthPartMap.get(id).body_par1);
                this.deltaX = sprite.getOriginX();
                this.deltaY = sprite.getOriginY();
            }
            this.health = sDB.HealthPartMap.get(id).health;
            //if (GlobalConstants.DEBUG_MODE)
            //    this.setScale(this.mBody.getWidthOrRadius()*2/this.getTextureRegion().getWidth());
        }
        else this.mBody = null;
    };

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public float GetHealth(){
        return health;
    }

    //public void SetNode (Node pNode) {this.worldDBNode = pNode;}
   //public Node GetNode() {return this.worldDBNode;}

    //@Override
    public int GetTypeOfBlast() {
        return sDB.HealthPartMap.get(sprite.getObjectID()).type_of_blast;
    }

    public float getX() { return sprite.getX();}
    public float getY() { return sprite.getY();}
    public float getA() { return sprite.getA();}
    public void setX(float pX) {sprite.setX(pX);}
    public void setY(float pY) {sprite.setY(pY);}
    public void setA(float pA) {sprite.setA(pA);}
    public float getCenterX() {return sprite.getCenterX();}
    public float getCenterY() {return sprite.getCenterY();}
    public float getOriginX() {return sprite.getOriginX();}
    public float getOriginY() {return sprite.getOriginY();}
    public void setOriginXY(float pX, float pY) {sprite.setOriginXY(pX, pY); }
    public float getSpeedX() {return sprite.getSpeedX();}
    public float getSpeedY() {return sprite.getSpeedY();}
    public float getSpeedA() {return sprite.getSpeedA();}
    public float getSpeedV() {return sprite.getSpeedV();}
    public void setSpeedXY(float pVX, float pVY) {sprite.setSpeedXY(pVX,pVY);}
    public void setSpeedVA(float pVV, float pVA) {sprite.setSpeedVA(pVV, pVA);}
    public void setSpeedA(float pVA) { sprite.setSpeedA(pVA);}
    public void setSpeedV(float pVV) { sprite.setSpeedV(pVV);}
    public void percentModifySpeed(float procent, float dt) {sprite.percentModifySpeed(procent, dt);}
    public void setScale (float pSX, float pSY) {sprite.setScale(pSX, pSY);}

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    //public TextureRegion getTextureRegion() { return sprite.getTextureRegion(); }
    public int getTypeOfObject()            { return sprite.getTypeOfObject();  }
    public int getObjectID()                { return sprite.getObjectID();      }
    public void setTypeOfObject(int i)      { sprite.setTypeOfObject(i);        }

    public byte update(float dt){
        r = sprite.update(dt);
        //r = 0;
        if (mBody != null)
            ResetBody();

        if (this.getTypeOfObject() < 0) //dead object
            r = 3;

        if (this.health <= 0 && this.getTypeOfObject() > 0){
            this.Kill();	// death
            r = 2;
        }
        return r;
    }


    public void render(SpriteBatch pSB) {  sprite.render(pSB);  }
    public void dispose() { sprite.dispose();}


    public void ReduceHealth(float healthToReduce1) {
        health -= healthToReduce1;
    };

    public void Kill(){
        health = 0;
        this.mBody = null;
        this.setTypeOfObject(-this.getTypeOfObject());
    }

    public Body GetBody() {
        return this.mBody;
    };

    // ===========================================================
    // Methods
    // ===========================================================

    public void ResetBody(){
        this.mBody.setX(this.getX() + deltaX);
        this.mBody.setY(this.getY() + deltaY);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
