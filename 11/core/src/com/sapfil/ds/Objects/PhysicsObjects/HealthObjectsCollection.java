package com.sapfil.ds.Objects.PhysicsObjects;

import com.badlogic.gdx.Gdx;
import com.sapfil.ds.Objects.GFXObjects.BasicObjectsCollection;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Prev edit: 16-10-2016
 * Last edit: 18-10-2016 extendins Bas-Obj-Col
 * Notes:
 ***********************************************/
public class HealthObjectsCollection extends BasicObjectsCollection {

    //public static BasicObjectsCollection mCorpses;

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static BasicObjectsCollection sBlasts;

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    public HealthObjectsCollection(){
        super();
        Gdx.app.log("Sapfil_Log", "New HealthObjectsCollection created");
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================
    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    // overriding basic method for accessing to 2- and 3- update commands
    public byte update (float dt){

        for (int i = 0; i<objectsArray.size; i++)
            switch (objectsArray.get(i).update(dt)) {
                case 2:   {                         // dead Haalth-object? needs blast
                    sBlasts.addNewObject(sDB.HealthPartMap.get(objectsArray.get(i).getObjectID()).type_of_blast,
                            objectsArray.get(i).getCenterX(),
                            objectsArray.get(i).getCenterY());
                }
                case 1:{}                           // dead Basic-object (animation stoped)
                case 3:{deleteObject(i); break;}    // dead Heath-object (Blast already created)
                default: {}                         // (case 0) object alive - nothing
            }
        return 0;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject (Integer id, float x, float y){ this.addNewObject(id, x, y, 0, 0); }
    public void addNewObject (Integer id, float x, float y, float vx, float vy){
        this.objectsArray.add(new HealthObject(id, x, y, vx, vy));
    }

    public void CreateCorpse(int deadType, float x, float y) {
        Gdx.app.log("Sapfil_Log", "deadType " + deadType);
       // switch (deadType){
        //    case 400: {this.mCorpses.AddNewObjectScaledAndRotated(1390, x, y, (float)rnd.nextInt(10)-5, (float)rnd.nextInt(20) + 10, 5, 1, 1, rnd.nextInt(360)); break;}
        //    default: {};
       //}
    }
}
