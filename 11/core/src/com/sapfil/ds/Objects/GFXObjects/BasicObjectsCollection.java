package com.sapfil.ds.Objects.GFXObjects;

/************************************************
 * Created by Sapfil on 13.01.2016.
 * Prev edit: 02-10-2016
 * Last edit: 18-10-2016 converting to Array<Iobject>
 * Notes:
 ***********************************************/

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.GlobalConstants;
import com.sapfil.ds.XML.DataBaseFromXML;

public class BasicObjectsCollection implements GlobalConstants {

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    protected Array<IBasicObject> objectsArray;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObjectsCollection() {
        Gdx.app.log("Sapfil_Log", "New BasicObjectsCollection created");
        objectsArray = new Array<IBasicObject>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getTotalObjects() {
        return (objectsArray.size);
    }

    public IBasicObject getObjectByIndex(int i) {
        return this.objectsArray.get(i);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt) {

        for (int i = 0; i < objectsArray.size; i++)
            switch (objectsArray.get(i).update(dt)) {
                case 0: break; // object alive
                case 1: {deleteObject(i); break; } // animation ended
                default: { }
            }
        return 0;
    }

    public void render(SpriteBatch pSB) {
        pSB.begin();
        for (IBasicObject nextSprite : objectsArray)
           nextSprite.render(pSB);
        pSB.end();
    }

    public void dispose() {
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject(Integer id, float x, float y) {
        if (sDB.BasicPartMap.get(id).fps != 0)
            this.objectsArray.add(new BasicAnimatedObject(id, x, y));
        else
            this.objectsArray.add(new BasicObject(id, x, y));
    }

    protected void deleteObject(int i) {
        this.objectsArray.get(i).dispose();
        objectsArray.removeIndex(i);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}