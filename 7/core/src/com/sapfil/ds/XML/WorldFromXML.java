package com.sapfil.ds.XML;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;
import com.sapfil.ds.GlobalConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/************************************************
 * Created by Sapfil on ... 01.2016.
 * Last edit: 02-10-2016
 * Notes: Mess-cleaning
 * TODO Memory-loss! Need asset-manager!!!
 ***********************************************/

public class WorldFromXML extends XmlReader
{
    // ===========================================================
    // Constants and static fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private int current_cellX = 0, current_cellY = 0;
    public final HashMap<Integer, HashMap<Integer, Integer>> SpaceCellList = new HashMap<Integer, HashMap<Integer, Integer>>();
    public Document rDoc;
    public NodeList cellsList;
    DocumentBuilder db;
    FileHandle inputFileHandler;
    InputStream mInputStream = null;
    //OutputStream mOutputStream = null;

    // ===========================================================
    // Constructors
    // ===========================================================

     public WorldFromXML()	throws Exception {
        db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        // TODO .exists NOT WORKIND in android-mode
        if (!GlobalConstants.CREATE_NEW_WORLD_ON_START) {
            inputFileHandler = Gdx.files.external("saved_world.xml");
            if (inputFileHandler.exists())
                mInputStream = inputFileHandler.read();
            if (mInputStream == null)                    {
                inputFileHandler = Gdx.files.internal("XML/worlds/world1.xml");
                    mInputStream = inputFileHandler.read();
                }

            rDoc = db.parse(mInputStream);
            cellsList = rDoc.getFirstChild().getChildNodes();

            for (int cell_number_in_XML = 0; cell_number_in_XML < cellsList.getLength(); cell_number_in_XML++)
                if (cellsList.item(cell_number_in_XML).getNodeType() == Node.ELEMENT_NODE) {
                    try { current_cellX = Integer.parseInt(cellsList.item(cell_number_in_XML).getAttributes().getNamedItem("x").getNodeValue()); } catch (Exception ex) { };
                    try { current_cellY = Integer.parseInt(cellsList.item(cell_number_in_XML).getAttributes().getNamedItem("y").getNodeValue()); } catch (Exception ex) { };

                    if (SpaceCellList.get(current_cellX) == null)
                        SpaceCellList.put(current_cellX, new HashMap<Integer, Integer>());

                    SpaceCellList.get(current_cellX).put(current_cellY, cell_number_in_XML);
                    Gdx.app.log("Sapfil_Log", "Cell added at " + current_cellX + " " + current_cellY);
                }
        }
        else {
            inputFileHandler = Gdx.files.internal("XML/worlds/empty_world.xml");
            mInputStream = inputFileHandler.read();
            rDoc = db.parse(mInputStream);
            cellsList = rDoc.getFirstChild().getChildNodes();

            Random rnd = new Random(System.currentTimeMillis());
            if (GlobalConstants.CREATE_HEX_WORLD_ON_START == false)
                for (int i = 0; i < GlobalConstants.START_ASTEROIDS_COUNT; i++) {
                    this.AddNode(rnd.nextInt(6) + 101,
                            rnd.nextInt(GlobalConstants.START_WORLD_SIZE * 2) - GlobalConstants.START_WORLD_SIZE,
                            rnd.nextInt(GlobalConstants.START_WORLD_SIZE * 2) - GlobalConstants.START_WORLD_SIZE);
                    Gdx.app.log("Sapfil_Log", "Creating asteroid " + i);
                }
            else {
                float p = GlobalConstants.WORLD_GRID_STEP_X * GlobalConstants.WORLD_GRID_STEP_Y*2
                        *(float) GlobalConstants.START_ASTEROIDS_COUNT/(float)(GlobalConstants.START_WORLD_SIZE* GlobalConstants.START_WORLD_SIZE);
                boolean step = false;
                for (int x = -GlobalConstants.START_WORLD_SIZE/ GlobalConstants.WORLD_GRID_STEP_X; x < GlobalConstants.START_WORLD_SIZE/ GlobalConstants.WORLD_GRID_STEP_X; x++) {
                    step = !step;
                    for (int y = -GlobalConstants.START_WORLD_SIZE/ GlobalConstants.WORLD_GRID_STEP_Y/2 ; y < GlobalConstants.START_WORLD_SIZE/ GlobalConstants.WORLD_GRID_STEP_Y/2 ; y++) {
                        if (Math.abs(x) > 3 || Math.abs(y) > 3)
                            if (rnd.nextFloat() < p) {
                                this.addAsteroid(rnd, x, y);
                                if (rnd.nextFloat() > 0.5f) {
                                    this.addAsteroid(rnd, x, y - 1);
                                    this.addAsteroid(rnd, x + 1, y  - 1);
                                    this.addAsteroid(rnd, x + 1, y );
                                    this.addAsteroid(rnd, x, y + 1);
                                    this.addAsteroid(rnd, x - 1, y  - 1);
                                    this.addAsteroid(rnd, x - 1, y );
                                }
                            }
                    }
                }
            }
        }

        this.saveWorld();

    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================
    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================
    // ===========================================================
    // Methods
    // ===========================================================

    public void DeleteNode(Node pNode)
    {
        pNode.getParentNode().removeChild(pNode);
    }

    // adding new object to rDoc
    public void AddNode(int id, int x, int y)
    {

        boolean cell_exist = false;
        int parent_cell = 1;

        // calculating coords of cell to paste object
        int cell_x = Math.round((float)x* GlobalConstants.WORLD_GRID_STEP_X/ GlobalConstants.CELL_ABS_SIZE_X)* GlobalConstants.CELL_ABS_SIZE_X;
        int cell_y = Math.round((float)y* GlobalConstants.WORLD_GRID_STEP_Y*2/ GlobalConstants.CELL_ABS_SIZE_Y)* GlobalConstants.CELL_ABS_SIZE_Y;

        // finding existing cell to paste object or creating new cell
        if (SpaceCellList.get(cell_x) != null)
            if (SpaceCellList.get(cell_x).get(cell_y) != null)
            {parent_cell = SpaceCellList.get(cell_x).get(cell_y); cell_exist = true;}
        if (!cell_exist)
        {
            if (SpaceCellList.get(cell_x) == null)
                SpaceCellList.put(cell_x, new HashMap<Integer, Integer>());
            org.w3c.dom.Element newCell = rDoc.createElement("NewCell");
            newCell.setAttribute("x",Integer.toString(cell_x));
            newCell.setAttribute("y",Integer.toString(cell_y));
            rDoc.getFirstChild().appendChild(newCell);
            parent_cell = rDoc.getFirstChild().getChildNodes().getLength() - 1;
            SpaceCellList.get(cell_x).put(cell_y, parent_cell);
            rDoc.getFirstChild().appendChild(rDoc.createTextNode("\n"));
        }

        boolean coordsNotEmpty = false;
        if (rDoc.getFirstChild().getChildNodes().item(parent_cell).hasChildNodes())
            for (int i = 0; i < rDoc.getFirstChild().getChildNodes().item(parent_cell).getChildNodes().getLength(); i++)
                if (rDoc.getFirstChild().getChildNodes().item(parent_cell).getChildNodes().item(i).hasAttributes())
                    if (rDoc.getFirstChild().getChildNodes().item(parent_cell).getChildNodes().item(i).getAttributes().getNamedItem("x").getNodeValue() == Integer.toString(x))
                        if (rDoc.getFirstChild().getChildNodes().item(parent_cell).getChildNodes().item(i).getAttributes().getNamedItem("y").getNodeValue() == Integer.toString(y)) {
                            Gdx.app.log("Sapfil_Log", "Found SAME object coords "
                                    + rDoc.getFirstChild().getChildNodes().item(parent_cell).getChildNodes().item(i).getAttributes().getNamedItem("x").getNodeValue() + " "
                                    + rDoc.getFirstChild().getChildNodes().item(parent_cell).getChildNodes().item(i).getAttributes().getNamedItem("y").getNodeValue() + " "
                                    + x + " " + y);
                            coordsNotEmpty = true;
                        }

        if (!coordsNotEmpty) {
            org.w3c.dom.Element newElement = rDoc.createElement("NewElement");
            newElement.setAttribute("id", Integer.toString(id));
            newElement.setAttribute("x", Integer.toString(x));
            newElement.setAttribute("y", Integer.toString(y));
            Gdx.app.log("Sapfil_Log", "Creating object: id " + id + " ,at " + x + " , " + y);
            //rDoc.getFirstChild().appendChild(newElement); adding new cell
            rDoc.getFirstChild().getChildNodes().item(parent_cell).appendChild(newElement);
            rDoc.getFirstChild().getChildNodes().item(parent_cell).appendChild(rDoc.createTextNode("\n"));
        }
        cellsList = rDoc.getFirstChild().getChildNodes();
    }

    public void saveWorld(){

        for (int cell_number_in_XML = 0; cell_number_in_XML < rDoc.getFirstChild().getChildNodes().getLength(); cell_number_in_XML++) {
            if (cellsList.item(cell_number_in_XML).getNodeType() == Node.TEXT_NODE)
                cellsList.item(cell_number_in_XML).setNodeValue("\n");
            for (int j = 0; j < rDoc.getFirstChild().getChildNodes().item(cell_number_in_XML).getChildNodes().getLength(); j++)
                if (cellsList.item(cell_number_in_XML).getChildNodes().item(j).getNodeType() == Node.TEXT_NODE)
                    cellsList.item(cell_number_in_XML).getChildNodes().item(j).setNodeValue("\t \n");
        }
        FileHandle outputFile = Gdx.files.external("saved_world.xml");

        if (!outputFile.exists())
            try { outputFile.file().createNewFile(); }
            catch (IOException e) { e.printStackTrace();{Gdx.app.log("Sapfil_Log", "ERROR in saving Level. " + e.getMessage()); } }

        Result output = new StreamResult(outputFile.file());
        Source input = new DOMSource(rDoc);
        Transformer transformer = null;

        try { transformer = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException e) { e.printStackTrace();{ Gdx.app.log("Sapfil_Log", "ERROR in saving Level. " + e.getMessage()); } }

        try { transformer.transform(input, output);
        } catch (TransformerException e) { e.printStackTrace(); {Gdx.app.log("Sapfil_Log", "ERROR in saving level. " + e.getMessage());  } }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    // for DEBUGGING
    private void addAsteroid(Random rnd, int x, int y){
        int i = rnd.nextInt(3);
        switch (i) {
            case 0: { this.AddNode(127, x, y );  break;  }
            case 1: { this.AddNode(128, x, y );  break;  }
            default:{ this.AddNode(129, x, y);           }
        };
    }


}

