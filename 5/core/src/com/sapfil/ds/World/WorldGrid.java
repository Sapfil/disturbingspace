package com.sapfil.ds.World;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.GlobalConstants;

/**
 * Created by Sapfil on 06.01.2016.
 */


public class WorldGrid {


    private boolean NFlag, SFlag, WFlag, EFlag, NWFlag, SWFlag, SEFlag, NEFlag;
    private int currentCenterX = 0, currentCenterY = 0;
    private float mTouchX = 0, mTouchY = 0;
    public boolean readyForRecalculatingEdges = false;
    public boolean EdgesAlreadyCalculated = false;
    public Array<WorldCell> mCells;

    public WorldGrid()
    {
        mCells = new Array<WorldCell>();
        this.AddWorldCell(0, 0);
        this.update(0, 0);
    }

    public void update(float pX, float pY) {
        this.NFlag = false; this.NWFlag = false; this.WFlag = false; this.SWFlag = false;
        this.SFlag = false; this.SEFlag = false; this.EFlag = false; this.NEFlag = false;

        for (int i = 0; i < this.mCells.size; i++){
            if (   Math.abs(mCells.get(i).GetCenterX() - pX) > GlobalConstants.CELL_ABS_SIZE_X*3/2
                    || Math.abs(mCells.get(i).GetCenterY() - pY) > GlobalConstants.CELL_ABS_SIZE_Y*3/2  ){
                Gdx.app.log("Sapfil_Log", "Detach: " + mCells.get(i).GetCenterX() + " " + mCells.get(i).GetCenterY());
                mCells.get(i).dispose();
                mCells.removeIndex(i);
                i--;
                continue;
            }

            // finding empty space aroud. If found - creating cells around.

            if (mCells.get(i).GetCenterX() - pX > GlobalConstants.CELL_ABS_SIZE_X/2){
                if (mCells.get(i).GetCenterY() - pY > GlobalConstants.CELL_ABS_SIZE_Y/2)
                    this.SEFlag = true;
                else
                {if (mCells.get(i).GetCenterY() - pY < -GlobalConstants.CELL_ABS_SIZE_Y/2)
                    this.NEFlag = true;
                else
                    this.EFlag = true;}
            }
            else if (mCells.get(i).GetCenterX() - pX < -GlobalConstants.CELL_ABS_SIZE_X/2){
                if (mCells.get(i).GetCenterY() - pY > GlobalConstants.CELL_ABS_SIZE_Y/2)
                    this.SWFlag = true;
                else
                {if (mCells.get(i).GetCenterY() - pY < -GlobalConstants.CELL_ABS_SIZE_Y/2)
                    this.NWFlag = true;
                else
                    this.WFlag = true;}
            }
            else{
                if (mCells.get(i).GetCenterY() - pY > GlobalConstants.CELL_ABS_SIZE_Y/2)
                    this.SFlag = true;
                else{
                    if (mCells.get(i).GetCenterY() - pY < - GlobalConstants.CELL_ABS_SIZE_Y/2)
                        this.NFlag = true;
                    else {
                        this.currentCenterX = mCells.get(i).GetCenterX();
                        this.currentCenterY = mCells.get(i).GetCenterY();
                    }
                }
            }
        }

        if (!this.NFlag) {this.AddWorldCell(currentCenterX, currentCenterY - GlobalConstants.CELL_ABS_SIZE_Y);     }
        if (!this.NWFlag){this.AddWorldCell(currentCenterX - GlobalConstants.CELL_ABS_SIZE_X, currentCenterY - GlobalConstants.CELL_ABS_SIZE_Y);     }
        if (!this.WFlag) {this.AddWorldCell(currentCenterX - GlobalConstants.CELL_ABS_SIZE_X, currentCenterY);                                       }
        if (!this.SWFlag){this.AddWorldCell(currentCenterX - GlobalConstants.CELL_ABS_SIZE_X, currentCenterY + GlobalConstants.CELL_ABS_SIZE_Y);     }
        if (!this.SFlag) {this.AddWorldCell(currentCenterX, currentCenterY + GlobalConstants.CELL_ABS_SIZE_Y);     }
        if (!this.SEFlag){this.AddWorldCell(currentCenterX + GlobalConstants.CELL_ABS_SIZE_X, currentCenterY + GlobalConstants.CELL_ABS_SIZE_Y);     }
        if (!this.EFlag) {this.AddWorldCell(currentCenterX + GlobalConstants.CELL_ABS_SIZE_X, currentCenterY);                                       }
        if (!this.NEFlag){this.AddWorldCell(currentCenterX + GlobalConstants.CELL_ABS_SIZE_X, currentCenterY - GlobalConstants.CELL_ABS_SIZE_Y);     }



    }

    private void AddWorldCell(final int pX, final int pY){
        //    if (pX == -540 && pY == 520)
        //        Log.d("Sapfil_Log", "WTF STOP");

        mCells.add(new WorldCell(pX /* /GlobalConstants.CELL_HEX_SIZE_X*/, pY/*/GlobalConstants.CELL_HEX_SIZE_Y*/));
        //this.attachChild(new GridNode(this.mGridActivity, this.mGridTextureRegion, this.mVertexBufferObjectManager, pX, pY));
        //((WorldCell) this.getLastChild()).FindNerbyCells();
        //((WorldCell) this.getLastChild()).CellsBordersMutualCopy();
//        ((WorldCell) this.getLastChild()).AddDecor();


        if (mCells.get(mCells.size-1).mAsteroidsLayer.getTotalObjects() != 0)
            Gdx.app.log("Sapfil_Log", "WorldCell created at " + pX + " " + pY + " with " + mCells.get(mCells.size-1).mAsteroidsLayer.getTotalObjects() + " children. Cells count: " + mCells.size);
        else
            Gdx.app.log("Sapfil_Log", "WorldCell created at " + pX + " " + pY + " without children. Cells count: " + mCells.size);

        //    if (((WorldCell) this.getLastChild()).mAsteroidsDecor != null)
        //        Log.d("Sapfil_Log", "WorldCell created at " + pX + " " + pY + " with " + ((WorldCell) this.getLastChild()).mAsteroidsDecor.getChildCount() + " decor children. Cells count: " + this.getChildCount());
        //    else
        //        Log.d("Sapfil_Log", "WorldCell created at " + pX + " " + pY + " without decor children. Cells count: " + this.getChildCount());
        //  if (this.getChildCount() == 9) {
        //int i = 0;
        //for (int j = 0; j < this.getLastChild().getChildCount(); j++)
        //    i = i + this.getChildByIndex(j).getChildCount();
        //      for (int j = 0; j<this.getChildCount(); j++) {
        //          ((WorldCell) this.getChildByIndex(j)).SetCellPositionType(currentCenterX, currentCenterY);
        //          ((WorldCell) this.getChildByIndex(j)).FindNerbyCells();
        //      }
        //Log.d("Sapfil_Log", "9 WorldCells exist with " + i + " sub-children");
        //  }
    }
    public void update(float dt){
        this.update(0,0);
        for (int i = 0; i < this.mCells.size; i++)
            mCells.get(i).update(dt);
    }

    public void render(SpriteBatch pSB){
        for (int i = 0; i < this.mCells.size; i++)
            mCells.get(i).render(pSB);
    }

    public void dispose(){
        for (int i = 0; i < this.mCells.size; i++)
            mCells.get(i).dispose();
    }

    public float getTouchX() {return this.mTouchX; };
    public float getTouchY() {return this.mTouchY; };



}
