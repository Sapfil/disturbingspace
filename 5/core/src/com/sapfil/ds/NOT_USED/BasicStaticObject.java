package com.sapfil.ds.NOT_USED;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.DisturbingSpace;
import com.sapfil.ds.XML.DataBaseFromXML;

/**
 * Created by Sapfil on 18.09.2016.
 */
public class BasicStaticObject extends Sprite {
// ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    //public float mX, mY, mA;
    public float mVX, mVY, mVA;

    public  int id;
    private int TypeOfObject;
    protected byte r;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicStaticObject(int id, float x, float y, float vx, float vy){
        super(sDB.BasicPartMap.get(id).textureRegion[0]);
        this.setX(x - this.getRegionWidth()/2);
        this.setY(y - this.getRegionHeight()/2);
        this.setRotation(0.0f);
        this.id = id;
        this.mVX = vx;
        this.mVY = vy;
        this.mVA = MathUtils.random( -100.0f, 100.0f);
    }


    public byte update(float dt){
        //r = 0;
        this.setX(this.getX() + mVX * dt);
        this.setY(this.getY() + mVY * dt);
        this.setRotation(this.getRotation() + mVA * dt);

        if (mVA != 0) {
            this.setRotation(this.getRotation() + mVA * dt);
            if (this.getRotation() < 0)
                this.setRotation(this.getRotation() + 360);
            else if (this.getRotation() > 360)
                this.setRotation(this.getRotation() - 360);
        }

        if (this.getX() <= 0 || this.getX() >= DisturbingSpace.WIDTH  - this.getRegionWidth())
            this.mVX *= -1;
        if (this.getX() <= 0 || this.getX() >= DisturbingSpace.HEIGHT - this.getRegionHeight())
            this.mVY *= -1;

        return 0;
    }



    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public int GetTypeOfObject(){
        return this.TypeOfObject;
    }

    public void SetTypeOfObject(int too){
        this.TypeOfObject = too;
    }

    public float GetSpeedX() {  return mVX;   }
    public float GetSpeedY() {  return mVY;   }

    public float GetSpeedA() {
        return mVA;
    }

    public float GetSpeedV() {
        return (float) Math.hypot((double) (-mVX), (double) (-mVY));
    }

    public void SetSpeedXY(float pVX, float pVY) {
        this.mVX = pVX; this.mVY = pVY;
    }

    public void SetSpeedVA(float pVV, float pVA) {
        //          mVX = pVV *(float) (Math.sin(Math.toRadians(this.getRotation())));
        //       mVY = pVV *(float) -(Math.cos(Math.toRadians(this.getRotation())));
        mVA = pVA;
    }

    public void SetSpeedA(float pVA) {
        mVA = pVA;
    }
    public void SetSpeedV(float pVV) {
        this.SetSpeedVA(pVV, this.GetSpeedA());
    }

    // @Override
    //  public float GetCenterX() {
    //      return this.getX() + this.getWidth()/2;
    // }

    // @Override
    //   public float GetCenterY() {
    //     return this.getY() + this.getHeight()/2;
    //  }

    public void PercentModifySpeed(float percent, float dt){
        this.mVX *= (percent - dt)/ percent;
        this.mVY *= (percent - dt)/ percent;
    }

    public int GetObjectID() {
        return this.id;
    }

    // ===========================================================
    // Methods
    // ===========================================================
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}

