package com.sapfil.ds.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.sapfil.ds.DisturbingSpace;
import com.sapfil.ds.Intersector;
import com.sapfil.ds.Objects.BasicObjectsCollection;
import com.sapfil.ds.Objects.HealthObjectsCollection;
import com.sapfil.ds.Objects.Hero;
import com.sapfil.ds.Objects.IHealthObject;
import com.sapfil.ds.World.WorldCell;
import com.sapfil.ds.World.WorldGrid;

/************************************************
 * Created by Sapfil on 16.09.2016.
 * Last edit: 02-10-2016 sBlasts added
 * Notes: Memory-loss check
 ***********************************************/
public class PlayState extends State {

    private Texture mBackTexture;
    private BitmapFont techInfoFont;
    private Vector3 currentFocus, targetFocus;
    //private float camSpeed = 100.0f;

    private WorldGrid mWorld;
    private Hero mHero;
    private Intersector mIntersector ;
    private BasicObjectsCollection mBlasts;

    public PlayState(GameStateManager pGSM) {
        super(pGSM);

        mWorld = new WorldGrid();
        mHero = new Hero();
        mIntersector = new Intersector();
        techInfoFont = new BitmapFont();
        mBlasts = new BasicObjectsCollection();
        HealthObjectsCollection.sBlasts = mBlasts;

        mBackTexture    = new Texture("Back2.png");

        currentFocus = new Vector3 (0, 0, 0);
        targetFocus  = new Vector3 (0, 0, 0);
        focusSet(currentFocus);
        camera.translate(mHero.regionWidth/2, mHero.regionHeight/2);
    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.justTouched()){
            mouse.x = Gdx.input.getX();
            mouse.y = Gdx.input.getY();
            camera.unproject(mouse);

            {
                // asteroid touch
                int j = -1;

                for (WorldCell nextCell : mWorld.mCells) {
                    j = mIntersector.CollContainsPoint(nextCell.mAsteroidsLayer, mouse.x, mouse.y);
                    if (j > -1) {
                        mHero.SetMoveTargetObject((IHealthObject) nextCell.mAsteroidsLayer.GetObjectByIndex(j));
                        break;
                    }
                }
                    if (j == -1)    // free space touch
                        mHero.SetMoveTargetPoint(mouse.x, mouse.y);
            }
        }
    }

    @Override
    public void update(float dt) {
        mWorld.update(mHero.mX, mHero.mY);  // updating world-grid - loading new world-cell and destroying far-cells
        mWorld.update(dt);                  // updating objects in every cell
        mHero.update(dt);
        mBlasts.update(dt);

        for (WorldCell nextCell: mWorld.mCells)
            mIntersector.CollWithSingleIntersect(nextCell.mAsteroidsLayer, mHero, -3);

        focusTranslate(mHero.mX, mHero.mY);
    }

    @Override
    public void render(SpriteBatch pSB) {

        camera.update();
        pSB.setProjectionMatrix(camera.combined);

        // *** back
        pSB.begin();
        pSB.draw(mBackTexture,  currentFocus.x - DisturbingSpace.WIDTH / 2 + mHero.regionWidth/2,
                                currentFocus.y - DisturbingSpace.HEIGHT / 2+mHero.regionHeight/2,
                                DisturbingSpace.WIDTH, DisturbingSpace.HEIGHT);
        pSB.end();

        mWorld.render(pSB);
        mHero.render(pSB);
        mBlasts.render(pSB);

        // *** tech-info
        pSB.begin();
        techInfoFont.draw(pSB, "FPS:   " + Integer.toString(Gdx.graphics.getFramesPerSecond()), currentFocus.x - DisturbingSpace.WIDTH / 2 + 52, currentFocus.y + DisturbingSpace.HEIGHT / 2);
        pSB.end();
    }

    @Override
    public void dispose() {
        mBackTexture.dispose();
        mWorld.dispose();
        mBlasts.dispose();
        techInfoFont.dispose();
    }

    private void focusSet(Vector3 v3){
        camera.translate(v3.x - currentFocus.x - DisturbingSpace.WIDTH/2, v3.y - currentFocus.y - DisturbingSpace.HEIGHT/2);
        currentFocus.x = v3.x;
        currentFocus.y = v3.y;
    }

    private void focusTranslate(float pX, float pY){
        camera.translate(pX-currentFocus.x, pY-currentFocus.y);
        currentFocus.x = pX;
        currentFocus.y = pY;
    }
}
