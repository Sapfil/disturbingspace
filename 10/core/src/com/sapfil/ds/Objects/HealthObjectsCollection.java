package com.sapfil.ds.Objects;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 16-10-2016
 * Notes:
 ***********************************************/
public class HealthObjectsCollection {

    //public static WorldFromXML sWorld;
    //public static BasicObjectsCollection mCorpses;
    public static BasicObjectsCollection sBlasts;
    // public HEXDecorObjectCollection mDecor;
    //public static Random rnd;

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    private float counter = 0.0f;
    public Array<HealthObject> healthObjectsArray;

    // ===========================================================
    // Constructors
    // ===========================================================

    public HealthObjectsCollection(){

        Gdx.app.log("Sapfil_Log", "New HealthObjectsCollection created");
        healthObjectsArray = new Array<HealthObject>();
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================
    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update (float dt){
        counter += dt;
        for (int i = 0; i<healthObjectsArray.size; i++)
            switch (healthObjectsArray.get(i).update(dt)) {
                case 0: break; // object alive

                case 2:   {
                    sBlasts.addNewObject(healthObjectsArray.get(i).GetTypeOfBlast(),
                            healthObjectsArray.get(i).getCenterX(),
                            healthObjectsArray.get(i).getCenterY() );
                    //sWorld.DeleteNode(((HealthObject) this.getChildByIndex(i)).GetNode());
                    healthObjectsArray.removeIndex(i);
                    break;
                }
                case 3:{healthObjectsArray.removeIndex(i); break;}

                default: {}
            }
        return 0;
    }

    public void render(SpriteBatch pSB){
        pSB.begin();
        for (HealthObject nextObject: healthObjectsArray)
            pSB.draw(nextObject.getKeyFrame(nextObject.lifeTime), nextObject.mX, nextObject.mY,
                    nextObject.originX,
                    nextObject.originY,
                    nextObject.regionWidth,
                    nextObject.regionHeight, 1, 1, nextObject.mA);

        pSB.end();
    }

    public void dispose(){
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject (Integer id, float x, float y){ this.addNewObject(id, x, y, 0, 0);  }
    public void addNewObject (Integer id, float x, float y, float vx, float vy){
        this.healthObjectsArray.add(new HealthObject(id, x, y, vx, vy));
    }

    public HealthObject GetObjectByIndex (int i){
        return this.healthObjectsArray.get(i);
    }


            //return 0;

  /*
        for (int i = 0; i < this.getChildCount(); i++)	{
            switch (((HealthObject) this.getChildByIndex(i)).Update(dt)){

                case 1: {
                    //sWorld.DeleteNode(((HealthObject) this.getChildByIndex(i)).GetNode());
                    this.detachChild(this.getChildByIndex(i));
                    break;
                }

                case 2:   {
                    sBlasts.AddNewObject(((HealthObject) this.getChildByIndex(i)).GetTypeOfBlast(),
                            ((HealthObject) this.getChildByIndex(i)).GetCenterX(),
                            ((HealthObject) this.getChildByIndex(i)).GetCenterY() );
                    //sWorld.DeleteNode(((HealthObject) this.getChildByIndex(i)).GetNode());
                    this.detachChild(this.getChildByIndex(i));
                    break;
                }

                case 3: {
                        if(((HealthObject) this.getChildByIndex(i)).GetNode() != null)
                             //sWorld.DeleteNode(((HealthObject) this.getChildByIndex(i)).GetNode());


                        this.CreateCorpse(((HealthObject) this.getChildByIndex(i)).GetTypeOfObject(),
                        this.getChildByIndex(i).getX() + this.getChildByIndex(i).getRotationCenterX(),
                        this.getChildByIndex(i).getY() + this.getChildByIndex(i).getRotationCenterY());

                    int _x = (int)this.getChildByIndex(i).getX();
                    int _y = (int)this.getChildByIndex(i).getY();

                    this.detachChild(this.getChildByIndex(i));

                   // this.mDecor.RecalculateEdgesArea(_x, _y, this);

                    break;} // off-screen
                case 0: {};
                default: {};
            };
        }
        */


    public void CreateCorpse(int deadType, float x, float y) {
        Gdx.app.log("Sapfil_Log", "deadType " + deadType);
       // switch (deadType){
        //    case 400: {this.mCorpses.AddNewObjectScaledAndRotated(1390, x, y, (float)rnd.nextInt(10)-5, (float)rnd.nextInt(20) + 10, 5, 1, 1, rnd.nextInt(360)); break;}
        //    default: {};
       //}
    }

    public int getTotalObjects(){
        return (healthObjectsArray.size);
    }
};
