package com.sapfil.ds.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.DisturbingSpace;
import com.sapfil.ds.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 15.09.2016.
 * Last edit: 02-10-2016
 * Notes: Memory-loss check
 * TODO check not-used methds, sort methods
 * TODO maybe needs more private fields and less public
 ***********************************************/

public class BasicAnimatedObject extends Animation implements IBasicObject {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    public float mX, mY, mA;                // mA is rotaion in degrees
    public float mVX, mVY, mVA;
    public float originX, originY;
    public float regionWidth, regionHeight;
    public float lifeTime = 0.0f;

    public  int id;
    private int TypeOfObject;
    public int fps;
    protected byte r;

    // ===========================================================
    // Constructors
    // ===========================================================
    public BasicAnimatedObject (int id){ this(id,0,0);}
    public BasicAnimatedObject(int id, float x, float y){  this(id, x, y, 0, 0);    }

    public BasicAnimatedObject(int id, float x, float y, float vx, float vy){
        super (sDB.BasicPartMap.get(id).duration, sDB.BasicPartMap.get(id).textureRegion);
        this.fps = sDB.BasicPartMap.get(id).fps;

        this.regionWidth= sDB.BasicPartMap.get(id).textureRegion[0].getRegionWidth();
        this.regionHeight = sDB.BasicPartMap.get(id).textureRegion[0].getRegionHeight();
        this.originX = this.regionWidth/2;
        this.originY = this.regionHeight/2;
        this.mX = x - this.originX;
        this.mY = y - this.originY;
        this.mA = 0.0f;                     // mA is rotaion in degrees
        this.id = id;
        this.mVX = vx;
        this.mVY = vy;
        this.mVA = 0;

        if ( sDB.BasicPartMap.get(id).fps != 0){
            this.setPlayMode(PlayMode.LOOP);
            if (id >= 900 ){
                this.setPlayMode(PlayMode.NORMAL);
            }
            if ( sDB.BasicPartMap.get(id).fps < 0 )
                this.setFrameDuration(this.getAnimationDuration() * MathUtils.random(0.75f, 1.5f));

            //this.setCullingEnabled(true);
        }


        this.TypeOfObject = sDB.BasicPartMap.get(id).type_of_object;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public int getObjectID() {
        return this.id;
    }
    public int getTypeOfObject(){ return this.TypeOfObject; }
    public void setTypeOfObject(int too){
        this.TypeOfObject = too;
    }

    public float getSpeedA() {
        return mVA;
    }

    public float getSpeedV() {
        return (float) Math.hypot((double) (-mVX), (double) (-mVY));
    }

    public void setSpeedXY(float pVX, float pVY) {
        this.mVX = pVX; this.mVY = pVY;
    }

    public void setSpeedVA(float pVV, float pVA) {
        mVX = -pVV *(float) (Math.sin(Math.toRadians(this.mA)));
        mVY = pVV *(float) (Math.cos(Math.toRadians(this.mA)));
        mVA = pVA;
    }

    public void setSpeedA(float pVA) {
        mVA = pVA;
    }
    public void setSpeedV(float pVV) {
        this.setSpeedVA(pVV, this.getSpeedA());
    }

    @Override
    public float getCenterX() {
        return mX + originX;
    }

    @Override
    public float getCenterY() {
        return mY + originY;
    }

    @Override
    public float getSpeedX() {
        return mVX;
    }

    @Override
    public float getSpeedY() {
        return mVY;
    }


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public byte update(float dt){

        this.mX += mVX * dt;
        this.mY += mVY * dt;
        this.mA += mVA * dt;
        lifeTime += dt;
        if (this.getPlayMode() == PlayMode.NORMAL && this.isAnimationFinished(lifeTime))
            return 1;
        return 0;
    }

    @Override
    public float getX() {
        return mX;
    }

    @Override
    public float getY() {
        return mY;
    }

    @Override
    public void setX(float pX) {
        mX = pX;
    }

    @Override
    public void setY(float pY) {
        mY = pY;
    }


    // ===========================================================
    // Methods
    // ===========================================================

    public void percentModifySpeed(float percent, float dt){
        this.mVX *= (percent - dt)/ percent;
        this.mVY *= (percent - dt)/ percent;
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}

