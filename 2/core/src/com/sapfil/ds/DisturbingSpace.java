package com.sapfil.ds;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.States.GameStateManager;
import com.sapfil.ds.States.MenuState;

public class DisturbingSpace extends ApplicationAdapter {

	public static final int WIDTH 	= 800;
	public static final int HEIGHT 	= 480;
	public static final String TITLE = "Disturbing Space demo 2";

	private GameStateManager mGSM;
	private SpriteBatch mBatch;
	//Texture img;
	
	@Override
	public void create () {
		mGSM = new GameStateManager();
		mBatch = new SpriteBatch();
		//img = new Texture("badlogic.jpg");
		Gdx.gl.glClearColor(0, 0, 0.5f, 1);
		mGSM.push(new MenuState(mGSM));
	}

	@Override
	public void render () {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		mGSM.update(Gdx.graphics.getDeltaTime());
		mGSM.render(mBatch);
	}
	
	@Override
	public void dispose () {
		mBatch.dispose();
		//img.dispose();
	}
}
