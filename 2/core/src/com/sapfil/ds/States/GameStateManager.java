package com.sapfil.ds.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/**
 * Created by Sapfil on 16.09.2016.
 */
public class GameStateManager {

    private BitmapFont techInfoFont;
    private Stack<State> States;

    public GameStateManager(){
        States = new Stack<State>();

    }

    public void push (State pState){
        States.push(pState);
    }

    public void pop(){
        States.pop().dispose();
    }

    public State peek(){
        return States.peek();
    }

    public void  set(State pState){
        this.pop();
        this.push(pState);
    }

    public void update (float dt){

        States.peek().update(dt);
        States.peek().inputHandler();
    }

    public void render(SpriteBatch pSB){
        States.peek().render(pSB);
    }

}
