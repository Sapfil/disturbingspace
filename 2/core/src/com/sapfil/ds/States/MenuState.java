package com.sapfil.ds.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.DisturbingSpace;

import java.awt.TextArea;

/**
 * Created by Sapfil on 16.09.2016.
 */
public class MenuState extends State {

    private Texture mBackTexture, mButtonTexture;


    public MenuState(GameStateManager pGSM) {
        super(pGSM);

        mBackTexture    = new Texture("StartScreen.png");
        mButtonTexture  = new Texture("Button.png");
    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.justTouched()){
            gsm.set(new PlayState(gsm));
            //dispose();
        }
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch pSB) {

        camera.update();

        pSB.begin();

        pSB.draw(mBackTexture, 0, 0, DisturbingSpace.WIDTH, DisturbingSpace.HEIGHT);
        pSB.draw(mButtonTexture, DisturbingSpace.WIDTH/2 - mButtonTexture.getWidth()/2, 130);

        pSB.end();

    }

    @Override
    public void dispose() {
        mButtonTexture.dispose();
        mBackTexture.dispose();
    }
}
