package com.sapfil.ds.Objects;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.sapfil.ds.DisturbingSpace;
import com.sapfil.ds.States.GameStateManager;

import java.util.Random;

/**
 * Created by Sapfil on 15.09.2016.
 */

public class BasicObject extends Sprite /*implements IBasicObject, GlobalConstants*/ {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    //public static DataBaseFromXML mDB;
    //public static VertexBufferObjectManager mVBOM;
    public static Camera mCamera;
    public static Random rnd = new Random();

    public float mVX, mVY, mVA;

    public  int id;
    private int TypeOfObject;
    protected byte r;

    //private Texture mTestSpriteTexture;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObject (Texture pTexture, float x, float y, float vx, float vy){
        super (pTexture);
        this.setX(x);
        this.setY(y);

/*
        public BasicObject(int id, float x, float y, float vx, float vy){
            this(id, 1, x, y, vx, vy);}
        public BasicObject(int id, float scale, float x, float y, float vx, float vy){
            super(x - mDB.BasicPartMap.get(id).textureRegion.getWidth() / 2  scale
                   y - mDB.BasicPartMap.get(id).textureRegion.getHeight() / 2  scale
                    mDB.BasicPartMap.get(id).textureRegion, mVBOM);
*/
        this.id = id;
        //          if (scale != 1){
        //              this.setScaleCenter(mDB.BasicPartMap.get(id).textureRegion.getWidth()/2, mDB.BasicPartMap.get(id).textureRegion.getHeight()/2);
        //              this.setScale(scale);
        //          }
        //          this.TypeOfObject = mDB.BasicPartMap.get(id).type_of_object;
        this.mVX = vx;
        this.mVY = vy;
        this.mVA = MathUtils.random( -100.0f, 100.0f);
  /*          if ( mDB.BasicPartMap.get(id).fps != 0){
                if (id >= 900 ){
                    this.animate(1000/mDB.BasicPartMap.get(id).fps, false); return;
                }
                if ( mDB.BasicPartMap.get(id).fps > 0)
                    this.animate(1000/mDB.BasicPartMap.get(id).fps);
                else{
                    this.animate(-1000/(mDB.BasicPartMap.get(id).fps + rnd.nextInt(10) - 5));
                }

                //this.setCullingEnabled(true);
            }
            */
    }


    public byte Update(float dt){
        //r = 0;
        this.setX(this.getX() + mVX * dt);
        this.setY(this.getY() + mVY * dt);

                 if (mVA != 0)
                    this.setRotation(this.getRotation() + mVA * dt);
                 if (this.getRotation() < 0)
                      this.setRotation(this.getRotation()+360);
                 else if (this.getRotation() > 360)
                     this.setRotation(this.getRotation() - 360);
        //        if (this.GetTypeOfObject() >=900)
        //            Log.d("Sapfil_Log", "WTF STOP!");


        //         if(!this.isAnimationRunning() && this.GetTypeOfObject() >= 900)
        //            r = 1;
        if (this.getX() <= 0 || this.getX() >= DisturbingSpace.WIDTH  - this.getWidth())
            this.mVX *= -1;
        if (this.getY() <= 0 || this.getY() >= DisturbingSpace.HEIGHT - this.getHeight())
            this.mVY *= -1;


        return 0;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public int GetTypeOfObject(){
        return this.TypeOfObject;
    }

    public void SetTypeOfObject(int too){
        this.TypeOfObject = too;
    }

    public float GetSpeedX() {  return mVX;   }
    public float GetSpeedY() {  return mVY;   }

    public float GetSpeedA() {
        return mVA;
    }

    public float GetSpeedV() {
        return (float) Math.hypot((double) (-mVX), (double) (-mVY));
    }

    public void SetSpeedXY(float pVX, float pVY) {
        this.mVX = pVX; this.mVY = pVY;
    }

    public void SetSpeedVA(float pVV, float pVA) {
        //          mVX = pVV *(float) (Math.sin(Math.toRadians(this.getRotation())));
        //       mVY = pVV *(float) -(Math.cos(Math.toRadians(this.getRotation())));
        mVA = pVA;
    }

    public void SetSpeedA(float pVA) {
        mVA = pVA;
    }
    public void SetSpeedV(float pVV) {
        this.SetSpeedVA(pVV, this.GetSpeedA());
    }

    // @Override
    public float GetCenterX() {
        return this.getX() + this.getWidth()/2;
    }

    // @Override
    public float GetCenterY() {
        return this.getY() + this.getHeight()/2;
    }

    public void PercentModifySpeed(float percent, float dt){
        this.mVX *= (percent - dt)/ percent;
        this.mVY *= (percent - dt)/ percent;
    }

    public int GetObjectID() {
        return this.id;
    }

    // ===========================================================
    // Methods
    // ===========================================================
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}

