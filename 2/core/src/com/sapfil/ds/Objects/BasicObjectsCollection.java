package com.sapfil.ds.Objects;

/**
 * Created by Sapfil on 13.01.2016.
 */



import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.GlobalConstants;
import com.badlogic.gdx.utils.Array;

//import ru.sapfil.AETest2.objects.BasicObject;
//import ru.sapfil.AETest2.xml.DataBaseFromXML;

  //      import com.sapfil.test3.GlobalConstants;
  //      import com.sapfil.test3.Objects.BasicObject;

public class BasicObjectsCollection extends Array<BasicObject> implements /*IObjectsCollection,*/ GlobalConstants {



    // ===========================================================
    // Constants
    // ===========================================================

    private final float testSpeed = 50.0f;

    // ===========================================================
    // Fields
    // ===========================================================

    private Texture mTestSpriteTexture;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObjectsCollection(){
        mTestSpriteTexture = new Texture("asteroid129.png");
            //Log.d("Sapfil_Log", "New BasicObjectCollection created");
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject (Integer id, float x, float y){
        this.createTestSprite(x, y);
    }

 /*   public void AddNewObjectScaledAndRotated(Integer id, float x, float y, float vx, float vy, int deformCenter, float pScaleX, float pScaleY, float pRotation){
        BasicObject currentObject = new BasicObject (id, x, y, vx, vy);
        float deformCenterX, deformCenterY;
        switch (deformCenter){
            case 2: {deformCenterX = currentObject.getWidth()/2; deformCenterY = 0;break;}
            case 5:
            default:{deformCenterX = currentObject.getWidth()/2; deformCenterY = currentObject.getHeight()/2;}
        }
        currentObject.setScaleCenter(deformCenterX, deformCenterY);
        currentObject.setRotationCenter(deformCenterX, deformCenterY);
        currentObject.setScale(pScaleX, pScaleY);
        currentObject.setRotation(pRotation);
        this.attachChild(currentObject);
    }

    public void AddNewObject (Integer id, float scale, float x, float y, float vx, float vy){
        this.attachChild(new BasicObject (id, scale, x, y, vx, vy));
    }
    */



    public byte update (float dt){
        for (BasicObject nextSprite: this){
            nextSprite.Update(dt);
        }

        /*for (int i = 0; i < this.getChildCount(); i++)	{
            switch (((BasicObject) this.getChildByIndex(i)).Update(dt)){
                case 1: {this.detachChild(this.getChildByIndex(i)); break;} // off-screen
                case 0: {};
                default: {};
            };
        }*/
        return 0;
    }

    public void render(SpriteBatch pSB){
        pSB.begin();
        for (BasicObject nextSprite: this){
            nextSprite.draw(pSB);
            //pSB.draw(nextSprite);
        }
        pSB.end();
    }

    public void dispose(){
        mTestSpriteTexture.dispose();
    }

    private void createTestSprite (float x, float y){
        BasicObject newSprite = new BasicObject(mTestSpriteTexture, x - 32, y - 32, MathUtils.random(-testSpeed, +testSpeed), MathUtils.random(-testSpeed,+testSpeed));
        this.add(newSprite);
    }
}