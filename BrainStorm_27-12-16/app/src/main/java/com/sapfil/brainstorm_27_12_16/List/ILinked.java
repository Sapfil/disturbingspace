package com.sapfil.brainstorm_27_12_16.List;

/************************************************
 * Created by Sapfil on 27.12.2016.
 * Last edit:
 * Notes:
 ***********************************************/
public interface ILinked<T> {

    public void printAll();

    public void addElement(T element);

    public T getElement(int numberInList);

    public void deleteElement(int numberInList);


}
