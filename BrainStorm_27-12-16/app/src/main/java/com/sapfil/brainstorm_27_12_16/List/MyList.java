package com.sapfil.brainstorm_27_12_16.List;

import org.w3c.dom.*;

/************************************************
 * Created by Sapfil on 27.12.2016.
 * Last edit:
 * Notes:
 ***********************************************/
public class MyList implements  ILinked<Node> {

    private int count = -1;
    private Node first, last;


    @Override
    public void printAll() {
        int currentElementNumber = 0;
        Node currentNode = first;
        while (currentElementNumber <= count){
            System.out.println(currentNode.getData());
            currentNode = currentNode.getNext();
            currentElementNumber++;
        }
    }

    @Override
    public void addElement(Node element) {
        if (count == -1)
            first = last = element;
        else
        {
            last.setNext(element);
            last = element;
        }
        count++;
    }

    @Override
    public Node getElement(int numberInList) {
        return this.getNodeByNumber(numberInList);
    }

    @Override
    public void deleteElement(int numberInList) {
        Node prevNode = this.getNodeByNumber(numberInList-1);
        if (prevNode!= null)
            if (prevNode.getNext() != null) {
                prevNode.setNext(prevNode.getNext().getNext());
                count--;
            }
    }

    private Node getNodeByNumber(int number){
        if (number > count)
            return null;
        if (number == count)
            return last;
        int currentElementNumber = 0;
        Node currentNode = first;
        while (currentElementNumber < number){
            currentNode = currentNode.getNext();
            currentElementNumber++;
        }
        return currentNode;
    }
}
