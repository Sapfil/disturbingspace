package com.sapfil.brainstorm_27_12_16.List;

import java.util.Objects;

/************************************************
 * Created by Sapfil on 27.12.2016.
 * Last edit:
 * Notes:
 ***********************************************/
public class Node {

    private Node next = null;
    private Object data;

    private Node(Builder builder) {
        this.next = builder.next;
        this.data = builder.data;
    }

    public Node getNext() {
        return next;
    }

    public Object getData() {
        return data;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static class Builder{

        private Node next;
        private Object data;

        public Builder(Object data) {
            this.data = data;
        }

        public Builder setNext(Node next){ this.next = next; return this;}
        public Builder setData(Object data){this.data = data; return this;}

        public Node build(){
            return new Node(this);
        }
    }
}
