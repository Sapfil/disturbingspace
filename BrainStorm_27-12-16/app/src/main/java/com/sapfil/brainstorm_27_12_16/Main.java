package com.sapfil.brainstorm_27_12_16;

import com.sapfil.brainstorm_27_12_16.List.MyList;
import com.sapfil.brainstorm_27_12_16.List.Node;

/************************************************
 * Created by Sapfil on 27.12.2016.
 * Last edit:
 * Notes:
 ***********************************************/
public class Main {

    public static void main(String[] args) {

        System.out.println("Hello world");
        System.out.println();

        MyList myList = new MyList();

        myList.addElement(new Node.Builder(new Object()).build());
        myList.addElement(new Node.Builder(new Object()).build());
        myList.addElement(new Node.Builder(new Object()).build());

        myList.printAll();

        System.out.println(myList.getElement(1).getData());

    }
}
