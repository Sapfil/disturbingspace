package com.sapfil.ds;

/**
 * Created by Sapfil on 10.09.2016.
 */
public interface HeroConstants {

    public static final float   HERO_CRASH_FREEZE = 0.5f;   // losing control time after collision
    public static final int     HERO_MAX_SPEED = 100;       // pixel in sec
    public static final int     HERO_RADIAL_SPEED = 200;    // grad in sec
    public static final int     HERO_ACCELERATION = 100;
    public static final float   HERO_BRAKING = 0.5f;

    public static final int     EMPTY_POINT_STOP_DISTANCE = 15; //distanceto point when braking starts
    public static final int     DRILL_MAX_DISTANCE = 70            ;
    public static final int     MAX_STAY_SPEED = 5;            // hero speed nearly "stand still" when STAY_AND_AIM-Status goes to STAY-Status.
    public static final float   AIM_ANGLE_COS   = 0.5f;         // hero will turn around withoun moving until angle(cos) to aim is less this float
    public static final float   AIM_ANGLE_SIN_MAX_ERROR = 0.05f; // hero may fly not directly to point. Direct fly without error will twitch sprite in every frame
}
