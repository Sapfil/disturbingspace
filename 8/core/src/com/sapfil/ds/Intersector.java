package com.sapfil.ds;

import com.badlogic.gdx.Gdx;
import com.sapfil.ds.Objects.HealthObject;
import com.sapfil.ds.Objects.HealthObjectsCollection;
import com.sapfil.ds.Objects.Hero;
import com.sapfil.ds.Objects.IHealthObject;


/**
 * Created by Sapfil on 16.01.2016.
 */
public class Intersector {
    //===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================
    public Intersector() {
        Gdx.app.log("Sapfil_Log", "Intersector created");
    };

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public void CollWithSingleIntersect (HealthObjectsCollection op1, IHealthObject op2, int typeOfIntersection){
        for (int i = 0; i < op1.getTotalObjects(); i++)
            this.SingleWithSingleIntersect(op1.healthObjectsArray.get(i), op2, typeOfIntersection);
    }

    public void SingleWithSingleIntersect (HealthObject op1, /*HEXDecorObjectCollection decor,*/ IHealthObject op2, int typeOfIntersection){
        if (op1.GetBody() != null && op2.GetBody() != null)
            if(op1.GetBody().collidesWith(op2.GetBody()))
                switch (typeOfIntersection){
                    case 0: {op1.ReduceHealth(op2.GetHealth()); op2.Kill(); break;}				 //op1 - target, op2 - bullet
                    case 1: {op2.ReduceHealth(op1.GetHealth()); op1.Kill(); break;} 			//op1 - bullet, op2 - target
                    case 2: {Repulse(op1, op2, true); break;}
                    case 3: {Repulse(op1, op2, false); ((Hero) op2).crashFreeze(HeroConstants.HERO_CRASH_FREEZE); break;}	// op1 - static, op2 - hero
                    case 4: {op2.Kill(); break;}  												//op1 - static, op2 - bullet

                    case -3: {Repulse(op1, op2, false); ((Hero) op2).crashFreeze(HeroConstants.HERO_CRASH_FREEZE);
                         op1.Kill(); break;}	// op1 - static, op2 - hero

                    default: {}
                }
    }

    public int CollContainsPoint (HealthObjectsCollection op1, float pX, float pY){
        for (int i = 0; i < op1.getTotalObjects(); i++)
            if (((HealthObject)op1.healthObjectsArray.get(i)).GetBody().contains(pX, pY))
                return i;
        return -1;
    };

    /*

        // recursion method for tree-construction objects
    // finding first child-object with body that contains some point
    public IBasicObject ObjectContainsPoint (IBasicObject op1, float pX, float pY){
        if (op1.GetTypeOfObject() != 0){
            if (((IHealthObject) op1).GetBody() != null)
                if (((IHealthObject)op1).GetBody().contains(pX, pY))
                    return (IHealthObject) op1;
        }
        return null;
    }
    */

    // recursion method for tree-construction objects
    // finding first child-object with body that intersect some body





    private void Repulse(IHealthObject op1, IHealthObject op2, boolean firstRepulse) {

        float v = (op1.getSpeedV() + op2.getSpeedV())/2;
        float deltaX = ((op2.getX()+op2.getCenterX()) - (op1.getX()+op1.getCenterX()));
        float deltaY = ((op2.getY()+op2.getCenterY()) - (op1.getY()+op1.getCenterY()));

        ((Hero)op2).SetStatus(Hero.Status.STAY);
        if (v < 5)
            v = 5;


        float angle = (float)Math.atan2(deltaX,deltaY);
        if (firstRepulse){
            op1.setSpeedXY((float)( -v * Math.sin(angle)),(float)( -v * Math.cos(angle)));
            op2.setSpeedXY((float) (v * Math.sin(angle)), (float) (v * Math.cos(angle)));
        }
        else{
            op2.setSpeedXY((float) (v * Math.sin(angle)), (float) (v * Math.cos(angle)));
            if(deltaY > 0)
                op2.setX(op2.getX() +3);
            // TODO WTF +3 ?????
        }
    }

    /*
    public void LayerWithSingleIntersect(Entity mLayer, Hero pHero, int collision_type) {
        for (int layer_child_counter = 0; layer_child_counter < mLayer.getChildCount(); layer_child_counter++)
            this.CollWithSingleIntersect((HealthObjectsCollection) (mLayer.getChildByIndex(layer_child_counter)), pHero, collision_type);
    }

    public void LayerWithPointIntersect(Entity mLayer, float pX, float pY) {
        for (int layer_child_counter = 0; layer_child_counter < mLayer.getChildCount(); layer_child_counter++)
            this.CollContainsPoint((HealthObjectsCollection) (mLayer.getChildByIndex(layer_child_counter)), pX, pY);
    }
    */


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
