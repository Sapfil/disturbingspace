package com.sapfil.ds.Objects;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 16-10-2016
 * Notes:
 ***********************************************/
public interface IHealthObject extends IBasicObject{
        // ===========================================================
        // Constants
        // ===========================================================

        // ===========================================================
        // Getter & Setter
        // ===========================================================

        Body mBody = null;

        float GetHealth();
        int GetTypeOfBlast();
        Body GetBody();

        // ===========================================================
        // Methods
        // ===========================================================

        void ResetBody();
        void ReduceHealth(float healthToReduce1);
        void Kill();
    }
