package com.sapfil.ds.Objects;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 16-10-2016
 * Notes:
 ***********************************************/
public interface IBasicObject {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    int getTypeOfObject();
    int getObjectID();
    void setTypeOfObject(int i);

    byte update(float dt);

    float getX();           float getY();
    void  setX(float pX);   void  setY(float pY);
    float getCenterX();     float getCenterY();

    float getSpeedX();      float getSpeedY();
    float getSpeedA();      float getSpeedV();
    void  setSpeedXY(float pVX, float pVY);
    void  setSpeedVA(float pVV, float pVA);
    void  setSpeedA(float pVA);
    void  setSpeedV(float pVV);

    void percentModifySpeed(float procent, float dt);

    //public boolean isInLevel();
}
