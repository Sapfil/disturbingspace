package com.sapfil.ds.Objects;

import org.w3c.dom.Node;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 09-10-2016
 * Notes:
 ***********************************************/
public class HealthObject extends BasicAnimatedObject implements IHealthObject {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private float deltaX, deltaY; // body offset from sprite corner-coordinate
    protected float health;
    private Body mBody;
    public Node worldDBNode = null;
    //private byte r;

    // ===========================================================
    // Constructors
    // ===========================================================

    // body_x and body_y *** 02-03-2013
    public HealthObject(int id){this(id,0,0);}
    public HealthObject(int id, float x, float y){this(id,x,y,0,0);}
    public HealthObject(int id, float x, float y, float vx, float vy){this(id, 1, x, y, vx, vy);}
    public HealthObject(int id, float scale, float x, float y, float vx, float vy)	{this(id, scale, 0, x, y, vx, vy, 0);}
    public HealthObject(int id, float scale, float angle,  float x, float y, float vx, float vy, float va){
        super (id, x, y, vx, vy);
        if (angle != 0)
            this.mA = angle;
        if (sDB.HealthPartMap.get(id).body_par1 != 0 ){
            if (sDB.HealthPartMap.get(id).bodyType == 0) {      //bodyType == 0 - circle body
                this.mBody = new Body(x, y, sDB.HealthPartMap.get(id).body_par1);
                this.deltaX = this.regionWidth / 2;
                this.deltaY = this.regionHeight / 2;
            }
            this.health = sDB.HealthPartMap.get(id).health;
            //if (GlobalConstants.DEBUG_MODE)
            //    this.setScale(this.mBody.getWidthOrRadius()*2/this.getTextureRegion().getWidth());
        }
        else this.mBody = null;
    };

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public float GetHealth(){
        return health;
    }

    //public void SetNode (Node pNode) {this.worldDBNode = pNode;}
   //public Node GetNode() {return this.worldDBNode;}

    //@Override
    public int GetTypeOfBlast() {
        return sDB.HealthPartMap.get(id).type_of_blast;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // returns 0 - object alive and onscreen, 1 - object offscreen, 2 - object must be killed in this frame, 3- deadObject
    public byte update(float dt){
        r = super.update(dt);
        //r = 0;
        if (mBody != null)
            ResetBody();

        if (this.getTypeOfObject() < 0) //dead object
            r = 3;

        if (this.health <= 0 && this.getTypeOfObject() > 0){
            this.Kill();	// death
            r = 2;
        }
        return r;
    }

    public void ReduceHealth(float healthToReduce1) {
        health -= healthToReduce1;
    };

    public void Kill(){
        health = 0;
        this.mBody = null;
        this.setTypeOfObject(-this.getTypeOfObject());
    }

    public Body GetBody() {
        return this.mBody;
    };

    // ===========================================================
    // Methods
    // ===========================================================

    public void ResetBody(){
        this.mBody.setX(this.mX + deltaX);
        this.mBody.setY(this.mY + deltaY);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
