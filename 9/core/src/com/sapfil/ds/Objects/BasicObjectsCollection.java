package com.sapfil.ds.Objects;

/************************************************
 * Created by Sapfil on 13.01.2016.
 * Last edit: 02-10-2016
 * Notes:
 ***********************************************/

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.GlobalConstants;
import com.sapfil.ds.XML.DataBaseFromXML;


public class BasicObjectsCollection implements /*IObjectsCollection,*/ GlobalConstants {

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    private float counter = 0.0f;
    private Array<BasicAnimatedObject> animatedObjectsArray;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObjectsCollection(){
        Gdx.app.log("Sapfil_Log", "New BasicObjectsCollection created");
        animatedObjectsArray = new Array<BasicAnimatedObject>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update (float dt){
        counter += dt;
        for (BasicAnimatedObject nextSprite: animatedObjectsArray)
            nextSprite.update(dt);
        return 0;
    }

    public void render(SpriteBatch pSB){
        pSB.begin();
        for (BasicAnimatedObject nextSprite: animatedObjectsArray)
            pSB.draw(nextSprite.getKeyFrame(counter, true), nextSprite.mX, nextSprite.mY,
                    nextSprite.originX,
                    nextSprite.originY,
                    nextSprite.regionWidth,
                    nextSprite.regionHeight, 1, 1, nextSprite.mA);

        pSB.end();
    }

    public void dispose(){
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject (Integer id, float x, float y){
            this.animatedObjectsArray.add(new BasicAnimatedObject(id, x, y));
    }

    public int getTotalObjects(){
        return (animatedObjectsArray.size);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}