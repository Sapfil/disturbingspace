package com.sapfil.ds.Objects;

/**
 * Created by Sapfil on 16.01.2016.
 */
public interface IObjectsCollection {
    // ===========================================================
    // Constants
    // ===========================================================

    public void AddNewObject(Integer id, float scale, float x, float y, float vx, float vy);

    public void AddNewObject(Integer id, int flyProgramID, int fly_program_shift);
};