package com.sapfil.ds.Objects;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 09-10-16
 * Notes:
 ***********************************************/

public class Body {
    // ===========================================================
    // Constants
    // ===========================================================

    enum BodyType {SPHERE, RECTANGLE};

    // ===========================================================
    // Fields
    // ===========================================================

    private float mX, mY;
    private int mWidthOrRadius, mHeightOrEllepticity;
    private BodyType mtype;

    // ===========================================================
    // Constructors
    // ===========================================================
    public Body(float pX, float pY, int pWidth_or_Radius) {      this(BodyType.SPHERE,pX,pY,pWidth_or_Radius,0);    }
    public Body(BodyType pType, float pX, float pY, int pWidth_or_Radius) {      this(pType,pX,pY,pWidth_or_Radius,0);    }
    public Body(BodyType pType, float pX, float pY, int pWidth_or_Radius, int pHeight_or_Ellipticity) {
        this.mtype = pType;
        this.mX = pX;
        this.mY = pY;
        this.mWidthOrRadius = pWidth_or_Radius;
        this.mHeightOrEllepticity = pHeight_or_Ellipticity;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    BodyType getType() {return this.mtype;}
    public float getX()   {return this.mX;}
    public float getY()   {return this.mY;}
    public int getWidthOrRadius() {return this.mWidthOrRadius;}
    public int getHeightOrEllepticity() {return  this.mHeightOrEllepticity;}

    void setX(float pX) {this.mX = pX; return;}
    void setY(float pY) {this.mY = pY; return;}

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================


    public boolean contains(float pX, float pY) {
        switch (this.mtype){
            case SPHERE: {if (Math.hypot((double)(pX-this.mX),(double)(pY-this.mY)) <= this.mWidthOrRadius)                                      return true; break;}
            case RECTANGLE: {if (this.mX <= pX && this.mX+this.mWidthOrRadius >= pX && this.mY <= pY && this.mY+this.mHeightOrEllepticity >= pY) return true; break;}
        }
        return false;
    }

    public boolean collidesWith(Body pOtherBody) {
        switch (this.mtype){
            case SPHERE:{
                switch (pOtherBody.getType()){
                    case SPHERE: { if(this.mX-pOtherBody.getX() <= this.getWidthOrRadius() + pOtherBody.getWidthOrRadius())
                                        if(this.mY-pOtherBody.getY() <= this.getWidthOrRadius() + pOtherBody.getWidthOrRadius())
                                            if (Math.hypot((double)(this.mX-pOtherBody.getX()),(double)(this.mY-pOtherBody.getY()))
                                            <= this.mWidthOrRadius + pOtherBody.getWidthOrRadius()) return true; break;}
                }
            }
        }
        // TODO other bodyTypes;
        return false;
    }
};
