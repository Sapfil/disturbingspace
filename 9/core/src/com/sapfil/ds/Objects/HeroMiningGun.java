package com.sapfil.ds.Objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

/************************************************
 * Created by Sapfil on 12.09.2016.
 * Last edit: 16-10-2016
 * Notes:
 ***********************************************/
public class HeroMiningGun{

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    private final int BIAS_X = 23;                  //energy beam start bias - from middle of hero sprite
    private final int BIAS_Y = 19;
    private final int BIAS2_X = BIAS_X * BIAS_X;

    private final float BIAS_ABS = (float)Math.sqrt(BIAS_X*BIAS_X + BIAS_Y*BIAS_Y);     // distance from middle of hero sprite to beam-start-point
    private final float BIAS_A = (float)MathUtils.atan2(BIAS_X, BIAS_Y) * MathUtils.radiansToDegrees; //basic angle between Y-axis and BIAS_ABS-line

    private final int RAY_RENDER_BIAS_X = 8;        //from low-left corner of sprite to beam-start-point
    private final int RAY_RENDER_BIAS_Y = 3;

    // ===========================================================
    // Fields
    // ===========================================================

    private float counter = 0.0f;

    private float mRotatoin;
    private float mTargetD, mTargetA;
    private float   parentOffsetX, parentOffsetY;

    private HealthObject pAim;

    private BasicAnimatedObject leftBeam, rightBeam, crossBeam;
    private float scaleLeft, scaleRight;
    private float angleLeft, angleRight;
    boolean isOn = false;

    // ===========================================================
    // Constructors
    // ===========================================================

    public HeroMiningGun(HealthObject pHero)  {

        parentOffsetX = pHero.regionWidth/2;
        parentOffsetY = pHero.regionHeight/2;

        leftBeam = new BasicAnimatedObject(23);
        rightBeam = new BasicAnimatedObject(24);
        crossBeam = new BasicAnimatedObject(25);

        this.Off();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public void setAimObject (HealthObject aim)
    {
        if (aim != null)
            pAim = aim;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt){
        counter += dt;
        leftBeam.update(dt);
        rightBeam.update(dt);
        crossBeam.update(dt);
        return 0;
    };

    public void render (SpriteBatch pSB){
        if (this.isOn) {
            pSB.begin();

            pSB.draw(leftBeam.getKeyFrame(counter, true), leftBeam.mX, leftBeam.mY,
                    RAY_RENDER_BIAS_X,
                    RAY_RENDER_BIAS_Y,
                    leftBeam.regionWidth,
                    leftBeam.regionHeight,
                    1, scaleLeft, leftBeam.mA);
            pSB.draw(rightBeam.getKeyFrame(counter, true), rightBeam.mX, rightBeam.mY,
                    RAY_RENDER_BIAS_X,
                    RAY_RENDER_BIAS_Y,
                    rightBeam.regionWidth,
                    rightBeam.regionHeight,
                    1, scaleRight, rightBeam.mA);
            pSB.draw(crossBeam.getKeyFrame(counter, true), pAim.getX() + pAim.getCenterX() - crossBeam.regionWidth/2, pAim.getY() + pAim.getCenterY() - crossBeam.regionHeight/2);

            pSB.end();
        }
    }

    public void dispose(){
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void On(){
        isOn = true;
    }

    public void Off() {
        isOn = false;
    }

    public void setParentParameters (float pX, float pY, float pA){
        leftBeam.setX(-RAY_RENDER_BIAS_X + pX + parentOffsetX + (BIAS_ABS * MathUtils.sinDeg( -pA - BIAS_A)));
        leftBeam.setY(-RAY_RENDER_BIAS_Y + pY + parentOffsetY + (BIAS_ABS * MathUtils.cosDeg(-pA - BIAS_A)));
        leftBeam.mA = pA + angleLeft;
        rightBeam.setX(-RAY_RENDER_BIAS_X + pX + parentOffsetX + (BIAS_ABS * MathUtils.sinDeg( -pA + BIAS_A)));
        rightBeam.setY(-RAY_RENDER_BIAS_Y + pY + parentOffsetY + (BIAS_ABS * MathUtils.cosDeg( -pA + BIAS_A)));
        rightBeam.mA = pA + angleRight;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public void SetParametersForDeformatiom(float pRotaion, float pTargetDiastance, float pTargetAngle){
        this.mRotatoin  = pRotaion;
        this.mTargetA   = pTargetAngle;
        this.mTargetD   = pTargetDiastance - BIAS_Y;

        // X-bias from hero middle-line aind line to target
        float BIAS_AIM    =  0;//(float)(Math.sin(Math.toRadians(mRotatoin - 180) + mTargetA)) * mTargetD;

        float Distance0 = (float)Math.sqrt(mTargetD * mTargetD + (BIAS_X - BIAS_AIM) * (BIAS_X - BIAS_AIM));
        angleLeft       = -MathUtils.atan2((BIAS_X -  BIAS_AIM), mTargetD) * MathUtils.radiansToDegrees;
        float Distance1 = (float)Math.sqrt(mTargetD * mTargetD + (BIAS_X + BIAS_AIM) * (BIAS_X + BIAS_AIM));
        angleRight      = MathUtils.atan2((BIAS_X + BIAS_AIM), mTargetD) * MathUtils.radiansToDegrees;

        //float center_x = this.getParent().getX()+this.getParent().getRotationCenterX();
        //float center_y = this.getParent().getY()+this.getParent().getRotationCenterY();

        scaleLeft    = Distance0/61;
        scaleRight   = Distance1/61;

        //Log.d("Sapfil_Log", mTargetA + " " + Math.toRadians(mRotatoin - 180));
        //float scaleW1   = (23 - scaleW)/23;
        //float scaleW2   = (23 + scaleW)/23;

        /*
        this.getChildByIndex(0).setScale(1, scaleH0);
        this.getChildByIndex(1).setScale(1, scaleH1);
        this.getChildByIndex(0).setRotation(mRotation + (float)(angle0/3.1415*180));
        this.getChildByIndex(1).setRotation(mRotation - (float)(angle1/3.1415*180));
        //this.getChildByIndex(1).setScale(scaleW2, scaleH);

        float center_x = mTargetD * (float)Math.sin(mTargetA);
        float center_y = mTargetD * (float)Math.cos(mTargetA);
        this.getChildByIndex(2).setX(24 - scaleW);
        this.getChildByIndex(2).setY(-mTargetD + 6);
        */
    }
}
