package com.sapfil.disturbingspace.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sapfil.disturbingspace.GameScreen;
import com.sapfil.disturbingspace.objects.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "Test Game";
		config.width = 800;
		config.height = 480;

		new LwjglApplication(new MyGdxGame(), config);
	}
}
