package com.sapfil.disturbingspace.objects;

//import android.util.Log;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

/**
 * Created by Sapfil on 15.09.2016.
 */

    public class BasicObject extends Rectangle /*implements IBasicObject, GlobalConstants*/ {
        // ===========================================================
        // Constants
        // ===========================================================

        // ===========================================================
        // Fields
        // ===========================================================

        public static Camera mCamera;
        public static Random rnd = new Random();

        public float mVX, mVY, mVA;

        public  int id;
        private int TypeOfObject;
        protected byte r;

        // ===========================================================
        // Constructors
        // ===========================================================

        public BasicObject (int id, float x, float y, float vx, float vy){

            super (x, y, 64, 64);

            this.id = id;
            this.mVX = vx;
            this.mVY = vy;
            this.mVA = 0;
        }


        public byte Update(float dt){

            this.setX(this.getX() + mVX * dt);
            this.setY(this.getY() + mVY * dt);
              if (this.getX() <= 0 || this.getX() >= 800-64)
                this.mVX *= -1;
            if (this.getY() <= 0 || this.getY() >= 480-64)
                this.mVY *= -1;
            return 0;
        }

        // ===========================================================
        // Getter & Setter
        // ===========================================================

        // ===========================================================
        // Methods for/from SuperClass/Interfaces
        // ===========================================================

        public int GetTypeOfObject(){
            return this.TypeOfObject;
        }

        public void SetTypeOfObject(int too){
            this.TypeOfObject = too;
        }

        public float GetSpeedX() {  return mVX;   }
        public float GetSpeedY() {  return mVY;   }

        public float GetSpeedA() {
            return mVA;
        }

        public float GetSpeedV() {
            return (float) Math.hypot((double) (-mVX), (double) (-mVY));
        }

        public void SetSpeedXY(float pVX, float pVY) {
            this.mVX = pVX; this.mVY = pVY;
        }

        public void SetSpeedVA(float pVV, float pVA) {
            mVA = pVA;
        }

        public void SetSpeedA(float pVA) {
            mVA = pVA;
        }
        public void SetSpeedV(float pVV) {
            this.SetSpeedVA(pVV, this.GetSpeedA());
        }

        // @Override
        public float GetCenterX() {
            return this.getX() + this.getWidth()/2;
        }

        // @Override
        public float GetCenterY() {
            return this.getY() + this.getHeight()/2;
        }

        public void PercentModifySpeed(float percent, float dt){
            this.mVX *= (percent - dt)/ percent;
            this.mVY *= (percent - dt)/ percent;
        }

        public int GetObjectID() {
            return this.id;
        }

        // ===========================================================
        // Methods
        // ===========================================================
        // ===========================================================
        // Inner and Anonymous Classes
        // ===========================================================
    }
