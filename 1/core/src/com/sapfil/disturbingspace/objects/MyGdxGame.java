package com.sapfil.disturbingspace.objects;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.disturbingspace.GameScreen;

/**
 * Created by Sapfil on 16.09.2016.
 */
public class MyGdxGame extends Game {

    public SpriteBatch batch;
    public BitmapFont font;

    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();

        this.setScreen(new MainMenu(this));

    }

    public void render(){
        super.render();
    }

    public void dispose(){
        super.dispose();
        batch.dispose();
        font.dispose();
    }
}
