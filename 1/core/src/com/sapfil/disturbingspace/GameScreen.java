package com.sapfil.disturbingspace;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.sapfil.disturbingspace.objects.BasicObject;
import com.sapfil.disturbingspace.objects.MyGdxGame;

import java.util.Iterator;

public class GameScreen implements Screen {
	//SpriteBatch batch;
	Texture ButtonTexture;
	Texture HeroTexture;
	Texture BackSpriteTexture;
	Sound tapSound;

	Vector3 touchPos;

	BasicObject Hero;
	Rectangle BackSprite;
	Rectangle Button;

	Array<BasicObject> testCollection;
	private static float testSpeed = 50.0f;

	//------------------------------------

	final MyGdxGame game;
	OrthographicCamera mCamera;
	boolean autoCreate = false;


	//@Override
	public GameScreen (final MyGdxGame gam) {
		this.game = gam;
		mCamera = new OrthographicCamera();
		mCamera.setToOrtho(false, 800, 480);


		touchPos = new Vector3();


		HeroTexture = new Texture("hero1.png");
		BackSpriteTexture = new Texture("Back2.png");
		tapSound = Gdx.audio.newSound(Gdx.files.internal("enemy_fire.mp3"));
		ButtonTexture = new Texture("Start_Autocreate_Button.jpg");



		//img = new Texture("badlogic.jpg");
		Hero = new BasicObject(1, 800/2 - 32,480/2 - 32,64,64);
		BackSprite = new Rectangle(0,0,800,600);
		Button = new Rectangle(700,380,100,100);

		testCollection = new Array<BasicObject>();
		this.CreateTestSprite(800/2-32, 480/2-32);
	}

	@Override
	public void render (float dt) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		mCamera.update();

		game.batch.setProjectionMatrix(mCamera.combined);

		game.batch.begin();
		game.batch.draw(BackSpriteTexture, 0 ,0, 800, 600);
		//batch.draw(HeroTexture, Hero.x, Hero.y);
		for (BasicObject nextSprite: testCollection){
			game.batch.draw(HeroTexture, nextSprite.x, nextSprite.y);
		}


		game.font.draw(game.batch, "FPS:   " + Integer.toString(Gdx.graphics.getFramesPerSecond()), 20, 460);
		game.font.draw(game.batch, "Rects: " + testCollection.size, 20, 420);
		game.batch.draw(ButtonTexture, Button.x, Button.y);

		game.batch.end();

		for (BasicObject nextSprite: testCollection){
			nextSprite.Update(dt);
		}


		if (autoCreate)
			if (Gdx.graphics.getFramesPerSecond() > 40){
				this.CreateTestSprite(touchPos.x, touchPos.y);
				if (Gdx.graphics.getFramesPerSecond() > 50){
					for (int i=0; i <5; i++)
						this.CreateTestSprite(MathUtils.random(0, 800-64), MathUtils.random(0, 480-64));
			}
		}

		if (Gdx.input.isTouched()){


			touchPos.x = Gdx.input.getX();
			touchPos.y = Gdx.input.getY();
			mCamera.unproject(touchPos);
			this.CreateTestSprite(touchPos.x, touchPos.y);
			//Hero.x = touchPos.x - Hero.getWidth()/2;
			//Hero.y = touchPos.y - Hero.getHeight()/2;
			tapSound.stop();
			tapSound.play();

			if (touchPos.x > 700 && touchPos.y > 380)
				autoCreate = true;
		}
	}

	@Override
	public void show() {

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose () {
		//batch.dispose();
		HeroTexture.dispose();
		BackSpriteTexture.dispose();
		tapSound.dispose();
		//img.dispose();
	}

	private void CreateTestSprite (float x, float y){
		BasicObject newSprite = new BasicObject(1, x - 32, y - 32, MathUtils.random(-testSpeed,+testSpeed), MathUtils.random(-testSpeed,+testSpeed));
		testCollection.add(newSprite);
	}
}