package com.sapfil.ds.World;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.GlobalConstants;
import com.sapfil.ds.Objects.BasicObjectsCollection;
import com.sapfil.ds.XML.DataBaseFromXML;
import com.sapfil.ds.XML.WorldFromXML;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Created by Sapfil on 24.01.2016.
 */
public class WorldCell extends Sprite {
    public static DataBaseFromXML sDB;
    public static WorldFromXML sWorldDB;

    private Texture mTechSprite;

    //public static Entity sAsteroidsLayer;
    public BasicObjectsCollection mAsteroidsLayer;

    //public int[][][] IDTable = new int[GlobalConstants.CELL_HEX_SIZE_X + 2][GlobalConstants.CELL_HEX_SIZE_Y/2 + 2][7];
    //public int IDTableLeftX, IDTableUpperY, TableX, TableY;
    //private byte mHEXoffset;

    private GlobalConstants.QUAD_Direction position_of_cell;

    // TODO: add comments to variables
    private int mNodeCenterX, mNodeCenterY, mHEXCenterX, mHEXCenterY, TableX, TableY, IDTableLeftX, IDTableUpperY;
    private byte mHEXoffset;
    NodeList currentCellObjectsList = null;
    private WorldCell pUpCell, pUpLeftCell, pLeftCell, pDownLeftCell, pDownCell, pDownRightCell, pRightCell, pUpRightCell;



    WorldCell(int pX, int pY) {
        this(pX, pY, GlobalConstants.QUAD_Direction.CENTER);
    }

    WorldCell(int pX, int pY, GlobalConstants.QUAD_Direction position) {
        //*** adding technical sprite
        super();
        mTechSprite = new Texture("Grid.png");
        mAsteroidsLayer = new BasicObjectsCollection();

        this.setRegion(mTechSprite);
        this.setPosition(pX, pY);

        pUpCell = null; pUpLeftCell = null; pLeftCell = null; pDownLeftCell = null;
        pDownCell = null; pDownRightCell = null; pRightCell = null; pUpRightCell = null;

        this.mNodeCenterX   = pX;           // in ABS
        this.mNodeCenterY   = pY;           // in ABS
        this.mHEXCenterX    = pX/ GlobalConstants.CELL_ABS_SIZE_X * GlobalConstants.CELL_HEX_SIZE_X; //in HEX
        this.mHEXCenterY    = pY/ GlobalConstants.CELL_ABS_SIZE_Y * GlobalConstants.CELL_HEX_SIZE_Y/2; // in HEX
        this.IDTableLeftX   = mHEXCenterX - GlobalConstants.CELL_HEX_SIZE_X/2;    // in HEX
        this.IDTableUpperY  = mHEXCenterY - GlobalConstants.CELL_HEX_SIZE_Y/2/2;    // in HEX

        this.position_of_cell = position;

        //*** if cell data exist in BIG_GREAT_WORLD_DATA_BASE - adding objects to game from database
        //*** if such cell doesnt exist in database - creating empty cell
        //mStaticObjectsTable = new int[GlobalConstants.CELL_HEX_SIZE_X][GlobalConstants.CELL_HEX_SIZE_Y];
        if (sWorldDB.SpaceCellList.get(pX) != null)
            if (sWorldDB.SpaceCellList.get(pX).get(pY) != null) {
                currentCellObjectsList = sWorldDB.cellsList.item(sWorldDB.SpaceCellList.get(pX).get(pY)).getChildNodes();

                for (int j = 0; j < currentCellObjectsList.getLength(); j++)
                    if (currentCellObjectsList.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        // *** reading nex object from DATA_BASE
                        int id = 0; int _x = 0;  int _y = 0;
                        try {id = Integer.parseInt(currentCellObjectsList.item(j).getAttributes().getNamedItem("id").getNodeValue());} catch (Exception ex) {};
                        try {_x = Integer.parseInt(currentCellObjectsList.item(j).getAttributes().getNamedItem("x").getNodeValue());} catch (Exception ex) {};
                        try {_y = Integer.parseInt(currentCellObjectsList.item(j).getAttributes().getNamedItem("y").getNodeValue());} catch (Exception ex) {};

                        // *** calculating IDTable coords and offset
                        TableX = (_x  - this.IDTableLeftX) + 1;
                        TableY = (_y  - this.IDTableUpperY) + 1;
                        if (TableX%2 != 0)   mHEXoffset = 0;
                        else                 mHEXoffset = 1;

                        // *** pasting it to static layer
                        mAsteroidsLayer.addNewObject(id, _x * GlobalConstants.WORLD_GRID_STEP_X, (_y * 2 + mHEXoffset) * GlobalConstants.WORLD_GRID_STEP_Y);
                    }
            }
    }


/*
    public void FindNerbyCells(){
        for (int i = 0; i < this.getParent().getChildCount(); i++){
            if (this.mNodeCenterX - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterX == GlobalConstants.CELL_ABS_SIZE_X){
                if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == GlobalConstants.CELL_ABS_SIZE_Y)
                {pUpLeftCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pDownRightCell = this;}
                else if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == -GlobalConstants.CELL_ABS_SIZE_Y)
                {pDownLeftCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pUpRightCell = this;}
                else if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == 0)
                {pLeftCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pRightCell = this;}
            }
            else if (this.mNodeCenterX - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterX == -GlobalConstants.CELL_ABS_SIZE_X){
                if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == GlobalConstants.CELL_ABS_SIZE_Y)
                {pUpRightCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pDownLeftCell = this;}
                else if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == -GlobalConstants.CELL_ABS_SIZE_Y)
                {pDownRightCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pUpLeftCell = this;}
                else if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == 0)
                {pRightCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pLeftCell = this;}
            }
            else if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == GlobalConstants.CELL_ABS_SIZE_Y &&
                    this.mNodeCenterX - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterX == 0)
            {pUpCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pDownCell = this;}
            else if (this.mNodeCenterY - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterY == -GlobalConstants.CELL_ABS_SIZE_Y &&
                    this.mNodeCenterX - ((WorldCell)this.getParent().getChildByIndex(i)).mNodeCenterX == 0)
            {pDownCell = (WorldCell)this.getParent().getChildByIndex(i);   ((WorldCell) this.getParent().getChildByIndex(i)).pUpCell = this;}
        }
    }
*/
    public void dispose() {
        mAsteroidsLayer.dispose();
        this.mTechSprite.dispose();
    }

    public void update (float dt){
        mAsteroidsLayer.update(dt);
    }

    public void render(SpriteBatch pSB){
        pSB.begin();
        pSB.draw(this.getTexture(), mNodeCenterX - this.getTexture().getWidth()/2, mNodeCenterY - this.getTexture().getHeight()/2);
        pSB.end();
        mAsteroidsLayer.render(pSB);
    }

    public int GetCenterX() {
        return this.mNodeCenterX;
    }

    ;
    public int GetCenterY() {
        return this.mNodeCenterY;}
    ;

    public GlobalConstants.QUAD_Direction GetCellPosition() {
        return this.position_of_cell;
    };

    public void SetCellPositionType(int pX, int pY){
        if (mNodeCenterX - pX >= GlobalConstants.CELL_ABS_SIZE_X){
            if (mNodeCenterY - pY >= GlobalConstants.CELL_ABS_SIZE_Y)            this.position_of_cell = GlobalConstants.QUAD_Direction.DOWN_RIGHT;
            else  {if (mNodeCenterY - pY <= -GlobalConstants.CELL_ABS_SIZE_Y)    this.position_of_cell = GlobalConstants.QUAD_Direction.UP_RIGHT;
            else                                                                 this.position_of_cell = GlobalConstants.QUAD_Direction.RIGHT;}
        }else if (mNodeCenterX - pX <= -GlobalConstants.CELL_ABS_SIZE_X){
            if (mNodeCenterY - pY >= GlobalConstants.CELL_ABS_SIZE_Y)            this.position_of_cell = GlobalConstants.QUAD_Direction.DOWN_LEFT;
            else{if (mNodeCenterY - pY <= -GlobalConstants.CELL_ABS_SIZE_Y)      this.position_of_cell = GlobalConstants.QUAD_Direction.UP_LEFT;
            else                                                                 this.position_of_cell = GlobalConstants.QUAD_Direction.LEFT;}
        }else{if (mNodeCenterY - pY >= GlobalConstants.CELL_ABS_SIZE_Y)          this.position_of_cell = GlobalConstants.QUAD_Direction.DOWN;
        else{if (mNodeCenterY - pY <= -GlobalConstants.CELL_ABS_SIZE_Y)      this.position_of_cell = GlobalConstants.QUAD_Direction.UP;
        else                                                             this.position_of_cell = GlobalConstants.QUAD_Direction.CENTER;
        }}
    }


}
