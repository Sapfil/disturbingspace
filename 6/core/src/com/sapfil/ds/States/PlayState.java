package com.sapfil.ds.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.sapfil.ds.DisturbingSpace;
import com.sapfil.ds.Objects.Hero;
import com.sapfil.ds.World.WorldGrid;

/**
 * Created by Sapfil on 16.09.2016.
 */
public class PlayState extends State {

    private Texture mBackTexture;
    //private Texture mHeroTexture;
    private BitmapFont techInfoFont;
    private Vector3 currentFocus;
    private Vector3 targetFocus;
    private float camSpeed = 100.0f;
    private WorldGrid mWorld;

    private Hero mHero;

    public PlayState(GameStateManager pGSM) {
        super(pGSM);

        mWorld = new WorldGrid();
        mHero = new Hero();
        techInfoFont = new BitmapFont();

        mBackTexture    = new Texture("Back2.png");
        //mHeroTexture    = new Texture("hero1.png");
        //ButtonTexture   = new Texture("Start_Autocreate_Button.jpg");

        currentFocus = new Vector3 (0, 0, 0);
        targetFocus  = new Vector3 (0, 0, 0);
        focusSet(currentFocus);
    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.justTouched()){
            mouse.x = Gdx.input.getX();
            mouse.y = Gdx.input.getY();
            camera.unproject(mouse);
            mHero.SetMoveTargetPoint(mouse.x, mouse.y);
            //targetFocus.x = mouse.x;
            //targetFocus.y = mouse.y;
        }
    }

    @Override
    public void update(float dt) {
        mWorld.update(dt);
        mHero.update(dt);

        //if (mHero.isMoving())
            focusTranslate(mHero.mX, mHero.mY);
    }

    @Override
    public void render(SpriteBatch pSB) {


        camera.update();
        pSB.setProjectionMatrix(camera.combined);

        pSB.begin();
        pSB.draw(mBackTexture, currentFocus.x - DisturbingSpace.WIDTH / 2, currentFocus.y - DisturbingSpace.HEIGHT / 2, DisturbingSpace.WIDTH, DisturbingSpace.HEIGHT);
        pSB.end();

        mWorld.render(pSB);
        mHero.render(pSB);

        pSB.begin();

        techInfoFont.draw(pSB, "FPS:   " + Integer.toString(Gdx.graphics.getFramesPerSecond()), currentFocus.x - DisturbingSpace.WIDTH / 2 + 20, currentFocus.y + DisturbingSpace.HEIGHT/2 - 20);

        pSB.end();
    }

    @Override
    public void dispose() {
        //mHeroTexture.dispose();
        mBackTexture.dispose();
        mWorld.dispose();
    }

    private void focusSet(Vector3 v3){
        camera.translate(v3.x - currentFocus.x - DisturbingSpace.WIDTH/2, v3.y - currentFocus.y - DisturbingSpace.HEIGHT/2);
        currentFocus.x = v3.x;
        currentFocus.y = v3.y;
    }

    private void focusTranslate(float pX, float pY){
        camera.translate(pX-currentFocus.x, pY-currentFocus.y);
        currentFocus.x = pX;
        currentFocus.y = pY;
    }
}
