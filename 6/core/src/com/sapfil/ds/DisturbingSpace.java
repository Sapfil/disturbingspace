package com.sapfil.ds;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.Objects.BasicAnimatedObject;
import com.sapfil.ds.Objects.BasicObjectsCollection;
//import com.sapfil.ds.NOT_USED.BasicStaticObject;
import com.sapfil.ds.States.GameStateManager;
import com.sapfil.ds.States.MenuState;
import com.sapfil.ds.States.PlayState;
import com.sapfil.ds.World.WorldCell;
import com.sapfil.ds.XML.DataBaseFromXML;
import com.sapfil.ds.XML.WorldFromXML;

public class DisturbingSpace extends ApplicationAdapter {

	public static final int WIDTH 	= 800;
	public static final int HEIGHT 	= 480;
	public static final String TITLE = "Disturbing Space demo 2";

	private GameStateManager mGSM;
	private SpriteBatch mBatch;
	private DataBaseFromXML mDB;
	private WorldFromXML mWorldDB;
	private AssetManager mAssetManager;
	//Texture img;

	@Override
	public void create () {

		mAssetManager = new AssetManager();
		//WorldFromXML.sExternailDir = this.getExternalFilesDir(null);

		try {
			mDB = new DataBaseFromXML(mAssetManager);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			mWorldDB = new WorldFromXML(mAssetManager);
		} catch (Exception e) {
			e.printStackTrace();
		}

		BasicAnimatedObject.sDB 	= mDB;
		//BasicStaticObject.sDB 		= mDB;
		BasicObjectsCollection.sDB 	= mDB;
		WorldCell.sDB				= mDB;

		WorldCell.sWorldDB			= mWorldDB;

		mGSM = new GameStateManager();
		mBatch = new SpriteBatch();
		//img = new Texture("badlogic.jpg");
		Gdx.gl.glClearColor(0, 0, 0.5f, 1);
		mGSM.push(new MenuState(mGSM));
	}

	@Override
	public void render () {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		mGSM.update(Gdx.graphics.getDeltaTime());
		mGSM.render(mBatch);

	}

	@Override
	public void dispose () {
		mBatch.dispose();
		//img.dispose();
	}
}
