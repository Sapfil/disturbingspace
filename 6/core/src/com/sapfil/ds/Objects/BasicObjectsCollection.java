package com.sapfil.ds.Objects;

/**
 * Created by Sapfil on 13.01.2016.
 */


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.GlobalConstants;
import com.sapfil.ds.XML.DataBaseFromXML;


public class BasicObjectsCollection implements /*IObjectsCollection,*/ GlobalConstants {

    // ===========================================================
    // Constants
    // ===========================================================

    private final float testSpeed = 50.0f;

    // ===========================================================
    // Fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    private float counter = 0.0f;
    private Array<BasicAnimatedObject> animatedObjectsArray;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObjectsCollection(){
        animatedObjectsArray = new Array<BasicAnimatedObject>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject (Integer id, float x, float y){
            this.animatedObjectsArray.add(new BasicAnimatedObject(id, x, y));
    }

    public void deleteAllObjects(){
        animatedObjectsArray.removeRange(0, animatedObjectsArray.size-1);
    }

    public byte update (float dt){
        counter += dt;
        for (BasicAnimatedObject nextSprite: animatedObjectsArray)
            nextSprite.update(dt);
        return 0;
    }

    public void render(SpriteBatch pSB){
        pSB.begin();
        for (BasicAnimatedObject nextSprite: animatedObjectsArray)
            pSB.draw(nextSprite.getKeyFrame(counter, true), nextSprite.mX, nextSprite.mY,
                     nextSprite.originX,
                     nextSprite.originY,
                     nextSprite.regionWidth,
                     nextSprite.regionHeight, 1, 1, nextSprite.mA);

        pSB.end();
    }

    public void dispose(){
    }

    private BasicAnimatedObject createTestSprite (int id, float x, float y){
        return new BasicAnimatedObject(id, x, y, MathUtils.random(-testSpeed, +testSpeed), MathUtils.random(-testSpeed,+testSpeed));
    }

    public int getTotalObjects(){
        return (animatedObjectsArray.size);
    }
}