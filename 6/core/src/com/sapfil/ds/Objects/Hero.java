package com.sapfil.ds.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.HeroConstants;

/**
 * Created by Sapfil on 27.12.2015.
 */
public class Hero extends BasicAnimatedObject {
    // ===========================================================
    // Constants
    // ===========================================================

    public enum Status {MOVINT_TO_TARGET, MOVING_TO_OBJECT, MOVING_TO_EMPTY_POINT, STAY_AND_AIM, STAY_AND_MINE, STAY}

    // ===========================================================
    // Fields
    // ===========================================================

    //private static float sX = 0;
    //private static float sY = 0;
    //private float speed = 100;

    private float target_D = 0, target_A, target_X, target_Y;
    private Status mCurrentStatus = Status.STAY;
    //private IHealthObject mAimObject;
    private int BrakeDistance = HeroConstants.EMPTY_POINT_STOP_DISTANCE;
    private float crashTimer = -1;
    //private HeroMiningGun mMiningGun = new HeroMiningGun();
    //private IHealthObject pTarget = null;

    // ===========================================================
    // Constructors
    // ===========================================================

    public Hero() {  super(1);
        //this.attachChild(mMiningGun);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    //public float getCenterX() {return (this.getX() + this.getRotationCenterX());}
   // public float getCenterY() {return (this.getY() + this.getRotationCenterY());}

    public void SetStatus (Status s){ mCurrentStatus = s; };

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public void render(SpriteBatch pSB){
        pSB.begin();
           pSB.draw(this.getKeyFrame(0, true), this.mX, this.mY,
                    this.originX,
                    this.originY,
                    this.regionWidth,
                    this.regionHeight, 1, 1, this.mA);

        pSB.end();
    }

    public byte update (float dt){
        byte super_return = super.update(dt);
        // TODO: Try to delete this
        //this.ResetBody();
        //Gdx.app.log("Sapfil_Log", "STATUS: " + this.mCurrentStatus );

        if (this.crashTimer < 0)

            switch (mCurrentStatus){
                case STAY: {
                    this.PercentModifySpeed(HeroConstants.HERO_BRAKING, dt);
                    SetSpeedA(0);
                    break;}
/*
                case STAY_AND_MINE:{
                    this.Target_A_Calculation();
                    this.Target_D_Calculation();
                    mMiningGun.SetParametersForDeformatiom(this.getRotation(), target_D, target_A);
                    mMiningGun.On();
                    if (this.GetSpeedV() > HeroConstants.MAX_STAY_SPEED / 2)
                        this.PercentModifySpeed(0.5f * HeroConstants.HERO_BRAKING, dt);
                    else
                        this.SetSpeedV(0);
                    pTarget.ReduceHealth(500 * dt);
                    if (pTarget.GetHealth() <= 0) {
                        //pTarget = null;
                        mCurrentStatus = Status.STAY;
                        mMiningGun.Off();
                    }
                    break;
                }
*/
                case STAY_AND_AIM:{

                    this.Target_A_Calculation();
                    this.Turn_To_Target();
                    this.PercentModifySpeed(0.5f * HeroConstants.HERO_BRAKING, dt);
                    if (this.GetSpeedA() == 0)
                        mCurrentStatus = Status.STAY_AND_MINE;
                    break;
                }

                case MOVING_TO_OBJECT:

                case MOVING_TO_EMPTY_POINT: {

    //                mMiningGun.Off();

                    this.Target_D_Calculation();
                    Gdx.app.log("Sapfil_Log", "Target_D: " + target_D + " Target_X: " + target_X + " Target_Y: " + target_Y  + " Hero_X: " + this.mX + " Hero_Y: " + this.mY);

                    if (target_D > BrakeDistance){
                        this.Target_A_Calculation();
                        this.Turn_To_Target(true);
                        //if (Math.cos(Math.toRadians(this.mA) + target_A) < -HeroConstants.AIM_ANGLE_COS)
                            this.SetSpeedV(this.GetSpeedV() + HeroConstants.HERO_ACCELERATION * dt);
                    }
                    else {
                        if (mCurrentStatus == Status.MOVING_TO_EMPTY_POINT)
                            mCurrentStatus = Status.STAY;
                        else
                            mCurrentStatus = Status.STAY_AND_AIM;
                    }
                }
            }
        else {
            this.crashTimer -= dt;
            // TODO: try to delete this
            mCurrentStatus = Status.STAY;
            this.PercentModifySpeed(HeroConstants.HERO_BRAKING, dt);
            SetSpeedA(0);
        }

        if (this.GetSpeedV() > HeroConstants.HERO_MAX_SPEED)
            this.SetSpeedV(HeroConstants.HERO_MAX_SPEED);

        return super_return;
    }
    // ===========================================================
    // Methods
    // ===========================================================

 /*   public void SetMoveTargetObject (IHealthObject pTargetObject){
        mCurrentStatus = Status.MOVING_TO_OBJECT;
        if (pTargetObject.GetBody().getType() == Body.BodyType.SPHERE) {
            target_X = pTargetObject.GetBody().getX();
            target_Y = pTargetObject.GetBody().getY();
            BrakeDistance = HeroConstants.DRILL_MAX_DISTANCE + pTargetObject.GetBody().getWidthOrRadius() + this.GetBody().getWidthOrRadius();
        }
        pTarget = pTargetObject;
    }
*/
    public void SetMoveTargetPoint (float pTarget_X, float pTarget_Y){
        mCurrentStatus = Status.MOVING_TO_EMPTY_POINT;
        BrakeDistance = HeroConstants.EMPTY_POINT_STOP_DISTANCE;
        target_X = pTarget_X;
        target_Y = pTarget_Y;
    }

    public void crashFreeze(float time){
        this.crashTimer = time;
    }

    private void Target_A_Calculation(){
        target_A = -(float) Math.atan2((double) (target_X - (this.mX + this.getKeyFrame(0).getRegionWidth() / 2)), (double) (target_Y - (this.mY + this.getKeyFrame(0).getRegionHeight()/2)));
    }

    private void Target_D_Calculation(){
        target_D = (float) Math.hypot((double) (this.mX + this.getKeyFrame(0).getRegionWidth() / 2 - target_X), (double) (this.mY + this.getKeyFrame(0).getRegionHeight() / 2 - target_Y));
    }

    private void Turn_To_Target () {this.Turn_To_Target(false);}
    private void Turn_To_Target(boolean directAiming){
        if (Math.abs(Math.sin(Math.toRadians(this.mA) - target_A)) > HeroConstants.AIM_ANGLE_SIN_MAX_ERROR || Math.cos(Math.toRadians(this.mA) - target_A) < 0 )
        {
            if (Math.sin(Math.toRadians(this.mA) - target_A) > 0)
                this.SetSpeedA(-HeroConstants.HERO_RADIAL_SPEED);
            else
                this.SetSpeedA( HeroConstants.HERO_RADIAL_SPEED);
        }
        else {
            this.SetSpeedA(0);
            if (directAiming){
                this.mA = ((float)(target_A * 180 / 3.141592));
            }
        }
        }

    public boolean isMoving(){
        switch (mCurrentStatus){
            case STAY:
            case STAY_AND_MINE:
            case STAY_AND_AIM: {return false;}
            case MOVING_TO_EMPTY_POINT:
            case MOVING_TO_OBJECT:
            case MOVINT_TO_TARGET:
            default: return true;
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
