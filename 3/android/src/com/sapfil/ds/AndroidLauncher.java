package com.sapfil.ds;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.sapfil.ds.DisturbingSpace;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		config.useCompass = false;
		config.useGyroscope = false;
		config.useAccelerometer = false;

		initialize(new DisturbingSpace(), config);
	}
}
