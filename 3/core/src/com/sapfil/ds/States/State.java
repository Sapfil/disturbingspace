package com.sapfil.ds.States;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.sapfil.ds.DisturbingSpace;

/**
 * Created by Sapfil on 16.09.2016.
 */
public abstract class State {

    protected OrthographicCamera camera;
    protected Vector3 mouse;
    protected GameStateManager gsm;
    //private     Vector3 touchCoords;

    public State(GameStateManager pGSM){
        this.gsm = pGSM;
        this.camera = new OrthographicCamera();
        camera.setToOrtho(false, DisturbingSpace.WIDTH, DisturbingSpace.HEIGHT);
        this.mouse = new Vector3();
    }

    protected abstract void inputHandler();
    public abstract void update (float dt);
    public abstract void render(SpriteBatch pSB);
    public abstract void dispose();

}
