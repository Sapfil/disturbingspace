package com.sapfil.ds.Objects;

/**
 * Created by Sapfil on 13.01.2016.
 */


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.GlobalConstants;

//import ru.sapfil.AETest2.objects.BasicObject;
//import ru.sapfil.AETest2.xml.DataBaseFromXML;

  //      import com.sapfil.test3.GlobalConstants;
  //      import com.sapfil.test3.Objects.BasicObject;

public class BasicObjectsCollection extends Array<BasicObject> implements /*IObjectsCollection,*/ GlobalConstants {



    // ===========================================================
    // Constants
    // ===========================================================

    private final float testSpeed = 50.0f;

    // ===========================================================
    // Fields
    // ===========================================================

    private Texture mTestSpriteTexture;
    private TextureRegion[] mTestTextureRegion;
    private float counter = 0.0f;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObjectsCollection(){
        mTestSpriteTexture = new Texture("enemy280.png");
        TextureRegion[][] tmpTextureregion = TextureRegion.split(mTestSpriteTexture, 60, 60);
        mTestTextureRegion = new TextureRegion[4 * 4];
        int index = 0;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                mTestTextureRegion[index++] = tmpTextureregion[i][j];

            //Log.d("Sapfil_Log", "New BasicObjectCollection created");
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject (Integer id, float x, float y){
        this.createTestSprite(x, y);
    }

 /*   public void AddNewObjectScaledAndRotated(Integer id, float x, float y, float vx, float vy, int deformCenter, float pScaleX, float pScaleY, float pRotation){
        BasicObject currentObject = new BasicObject (id, x, y, vx, vy);
        float deformCenterX, deformCenterY;
        switch (deformCenter){
            case 2: {deformCenterX = currentObject.getWidth()/2; deformCenterY = 0;break;}
            case 5:
            default:{deformCenterX = currentObject.getWidth()/2; deformCenterY = currentObject.getHeight()/2;}
        }
        currentObject.setScaleCenter(deformCenterX, deformCenterY);
        currentObject.setRotationCenter(deformCenterX, deformCenterY);
        currentObject.setScale(pScaleX, pScaleY);
        currentObject.setRotation(pRotation);
        this.attachChild(currentObject);
    }

    public void AddNewObject (Integer id, float scale, float x, float y, float vx, float vy){
        this.attachChild(new BasicObject (id, scale, x, y, vx, vy));
    }
    */



    public byte update (float dt){
        counter += dt;
        for (BasicObject nextSprite: this){
            nextSprite.Update(dt);
        }

        /*for (int i = 0; i < this.getChildCount(); i++)	{
            switch (((BasicObject) this.getChildByIndex(i)).Update(dt)){
                case 1: {this.detachChild(this.getChildByIndex(i)); break;} // off-screen
                case 0: {};
                default: {};
            };
        }*/
        return 0;
    }

    public void render(SpriteBatch pSB){
        pSB.begin();
        //nextSprite.draw(pSB);
        for (BasicObject nextSprite: this) {
            pSB.draw(nextSprite.getKeyFrame(counter, true), nextSprite.mX, nextSprite.mY, 30, 30, 60, 60, 1, 1, nextSprite.mA);
            //pSB.draw(nextSprite.getKeyFrame(counter, true), nextSprite.mX ,nextSprite.mY);
        }
        pSB.end();
    }

    public void dispose(){
        mTestSpriteTexture.dispose();
    }

    private void createTestSprite (float x, float y){
        this.add(new BasicObject(mTestTextureRegion, x-30, y-30, MathUtils.random(-testSpeed, +testSpeed), MathUtils.random(-testSpeed,+testSpeed)));
    }
}