package com.sapfil.ds.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.sapfil.ds.DisturbingSpace;
import com.sapfil.ds.Objects.BasicObjectsCollection;

/**
 * Created by Sapfil on 16.09.2016.
 */
public class PlayState extends State {

    private Texture mBackTexture;
    private Texture mHeroTexture;
    private BasicObjectsCollection mSpritesCollection;
    private Vector3 mHeroPosition;
    private BitmapFont techInfoFont;

    private Texture ButtonTexture;

    private boolean autoCreate = false;
    private int stressTestsPassed = 0;
    private float averageRects;


    public PlayState(GameStateManager pGSM) {
        super(pGSM);

        mSpritesCollection = new BasicObjectsCollection();
        techInfoFont = new BitmapFont();

        mBackTexture    = new Texture("Back2.png");
        mHeroTexture    = new Texture("hero1.png");
        ButtonTexture   = new Texture("Start_Autocreate_Button.jpg");

        mHeroPosition = new Vector3(DisturbingSpace.WIDTH/2 - mHeroTexture.getWidth()/2, DisturbingSpace.HEIGHT/2 - mHeroTexture.getHeight()/2, 0);
    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.isTouched()){
            mouse.x = Gdx.input.getX();
            mouse.y = Gdx.input.getY();
            //touchPos.x = ;
            //touchPos.y = ;
            //mCamera.unproject(touchPos);
            camera.unproject(mouse);
            mSpritesCollection.addNewObject(MathUtils.random(100,103), mouse.x, mouse.y);
            mHeroPosition.set(mouse.x, mouse.y, 0);

            if (mouse.x > 700 && mouse.y > 380)
                autoCreate = true;

            //Hero.x = touchPos.x - Hero.getWidth()/2;
            //Hero.y = touchPos.y - Hero.getHeight()/2;
            //tapSound.stop();
            //tapSound.play();
        }

    }

    @Override
    public void update(float dt) {
        if (autoCreate)
            if (Gdx.graphics.getFramesPerSecond() > 40){
                this.mSpritesCollection.addNewObject(MathUtils.random(100,103),  MathUtils.random(60, 800 - 60), MathUtils.random(60, 480-60));
                if (Gdx.graphics.getFramesPerSecond() > 50){
                    for (int i=0; i <5; i++)
                        this.mSpritesCollection.addNewObject(MathUtils.random(100,103), MathUtils.random(60, 800 - 60), MathUtils.random(60, 480 - 60));
                }
            }

        mSpritesCollection.update(dt);
    }

    @Override
    public void render(SpriteBatch pSB) {

        camera.update();
        pSB.setProjectionMatrix(camera.combined);

        pSB.begin();
        pSB.draw(mBackTexture, 0, 0, DisturbingSpace.WIDTH, DisturbingSpace.HEIGHT);
        pSB.end();

        mSpritesCollection.render(pSB);

        pSB.begin();
        pSB.draw(mHeroTexture, mHeroPosition.x, mHeroPosition.y);
        pSB.end();

        pSB.begin();
        pSB.draw(ButtonTexture, 700, 380);
        techInfoFont.draw(pSB, "FPS:   " + Integer.toString(Gdx.graphics.getFramesPerSecond()), 20, 460);
        techInfoFont.draw(pSB, "Rects: " + mSpritesCollection.size, 20, 420);
        techInfoFont.draw(pSB, "Average Rects: " + this.averageRects, 20, 380 );
        pSB.end();
    }

    @Override
    public void dispose() {
        mSpritesCollection.dispose();
        mHeroTexture.dispose();
        mBackTexture.dispose();
    }
}
