package com.sapfil.ds;

/**
 * Created by Sapfil on 13.01.2016.
 */
public interface GlobalConstants{


    //===========================================================
// Final Fields
// ===========================================================

    enum QUAD_Direction {UP, UP_LEFT, LEFT, DOWN_LEFT, DOWN, DOWN_RIGHT, RIGHT, UP_RIGHT, CENTER};

    public static final int CAMERA_WIDTH = 480;
    public static final int CAMERA_HEIGHT = 320;

    public static final int LEVEL_WIDTH = 480;
    public static final int LEVEL_HEIGHT = 480;
    public static final int WOBBLE_BORDER = 80;
    public static final int DEATH_LINE = 256;

    public static final int CRAZY_PARTICLE_OFFSET_X = -18;
    public static final int CRAZY_PARTICLE_OFFSET_Y = -18;

    public static final boolean DEBUG_MODE = false;
    public static final boolean CREATE_NEW_WORLD_ON_START = false;
    public static final boolean CREATE_HEX_WORLD_ON_START = true;
    public static final int START_ASTEROIDS_COUNT = 50;
    public static final int START_WORLD_SIZE = 1000;

    public static final byte WORLD_GRID_STEP_X = 45;
    public static final byte WORLD_GRID_STEP_Y = 26;

    public static final byte CELL_HEX_SIZE_X = 12;
    public static final byte CELL_HEX_SIZE_Y = 20;
    public static final int CELL_ABS_SIZE_X = 540;
    public static final int CELL_ABS_SIZE_Y = 520;
    //public static final int CELL_ABS_SIZE_X = CELL_HEX_SIZE_X * WORLD_GRID_STEP_X;
    // public static final int CELL_ABS_SIZE_Y = CELL_HEX_SIZE_Y * WORLD_GRID_STEP_Y;
//===========================================================
// Methods
// ===========================================================
};

