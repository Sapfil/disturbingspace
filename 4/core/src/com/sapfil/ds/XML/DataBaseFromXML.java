package com.sapfil.ds.XML;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.XmlReader;
import com.sapfil.ds.GlobalConstants;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
//import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class DataBaseFromXML extends XmlReader
{

    private String currentParent = "";

    private Texture currentTexture;
    public final HashMap<Integer, BasicPartSample> BasicPartMap 				= new HashMap<Integer, BasicPartSample>();
    //public final HashMap<Integer, HealthPartSample>  HealthPartMap				= new HashMap<Integer, HealthPartSample>();

    public DataBaseFromXML (AssetManager pAssetManager)	throws Exception {

        FileHandle inputFile = Gdx.files.internal("XML/dataBase.xml");
        XmlReader.Element root = this.parse(inputFile);

        for (int i = 0; i < root.getChildCount(); i++) {
            String chapterName = root.getChild(i).getName();
            currentTexture = new Texture(chapterName + ".png");
            for (int j = 0; j < root.getChild(i).getChildCount(); j++) {

                int id = root.getChild(i).getChild(j).getIntAttribute("id", 0);
                int too = root.getChild(i).getChild(j).getIntAttribute("too", 0);
                int texture_x = root.getChild(i).getChild(j).getIntAttribute("x", 0);
                int texture_y = root.getChild(i).getChild(j).getIntAttribute("y", 0);
                int width = root.getChild(i).getChild(j).getIntAttribute("w", 0);
                int height = root.getChild(i).getChild(j).getIntAttribute("h", 0);
                int fps = root.getChild(i).getChild(j).getIntAttribute("fps", 0);
                int framesInRow = root.getChild(i).getChild(j).getIntAttribute("fw", 1);
                int framesInColomn = root.getChild(i).getChild(j).getIntAttribute("fh", 1);

                BasicPartMap.put((Integer) id, new BasicPartSample(id, too,
                        fps, framesInRow, framesInColomn,
                        texture_x, texture_y, width, height, currentTexture));

            }
        }
    }

    // my class for saving object-samples-information
    public class BasicPartSample{
        public int id, type_of_object;
        public TextureRegion[] textureRegion;
        public int fps;


        BasicPartSample (int _id, int _too,
                         int _fps, int _framesInRow,  int _framesInColomn,
                         int _texture_x, int _texture_y, int _width, int _height, 	Texture _currentTexture)	{
            this.type_of_object = _too;
            this.id = _id;
            this.fps = _fps;
            TextureRegion tmpTextureRegion = new TextureRegion(_currentTexture, _texture_x, _texture_y, _width, _height);
            TextureRegion[][] tmpRegiosMassive = tmpTextureRegion.split((int)(_width/_framesInRow), (int)(_height/_framesInRow));
            textureRegion = new TextureRegion[_framesInRow * _framesInRow];

            for (int i = 0; i < _framesInRow; i++)
                for (int j = 0; j < _framesInColomn; j++)
                    textureRegion[i*_framesInRow + j] = tmpRegiosMassive[i][j];
        }
    }
/*
    public class HealthPartSample {

        public int id;
        public int bodyType, body_x, body_y, body_par1, body_par2;
        public int health;
        public int type_of_blast;
        HealthPartSample(int _id, int bodyType, int _body_x, int _body_y,
                         int _body_width, int _body_height, int _health, int _tob){
            this.bodyType = bodyType;
            this.id = _id;
            this.type_of_blast = _tob;
            this.body_x = _body_x; this.body_y = _body_y;
            this.body_par1 = _body_width; this.body_par2 = _body_height;
            this.health = _health;
        }
    }
*/
}


