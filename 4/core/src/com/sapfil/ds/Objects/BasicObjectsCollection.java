package com.sapfil.ds.Objects;

/**
 * Created by Sapfil on 13.01.2016.
 */

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.GlobalConstants;

public class BasicObjectsCollection extends Array<BasicObject> implements /*IObjectsCollection,*/ GlobalConstants {



    // ===========================================================
    // Constants
    // ===========================================================

    private final float testSpeed = 50.0f;

    // ===========================================================
    // Fields
    // ===========================================================

    private float counter = 0.0f;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObjectsCollection(){
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject (Integer id, float x, float y){
        this.createTestSprite(id, x, y);
    }

    public byte update (float dt){
        counter += dt;
        for (BasicObject nextSprite: this){
            nextSprite.Update(dt);
        }
        return 0;
    }

    public void render(SpriteBatch pSB){
        pSB.begin();
        for (BasicObject nextSprite: this) {
            if (nextSprite.fps != 0)
                pSB.draw(nextSprite.getKeyFrame(counter, true), nextSprite.mX, nextSprite.mY, 30, 30, 60, 60, 1, 1, nextSprite.mA);
            else
                pSB.draw(nextSprite.getKeyFrame(0), nextSprite.mX, nextSprite.mY);
        }
        pSB.end();
    }

    public void dispose(){
    }

    private void createTestSprite (int id, float x, float y){
        this.add(new BasicObject(id, x-30, y-30, MathUtils.random(-testSpeed, +testSpeed), MathUtils.random(-testSpeed,+testSpeed)));
    }
}