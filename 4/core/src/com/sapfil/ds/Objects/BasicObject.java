package com.sapfil.ds.Objects;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.DisturbingSpace;
import com.sapfil.ds.XML.DataBaseFromXML;

import java.util.Random;

/**
 * Created by Sapfil on 15.09.2016.
 */

public class BasicObject extends Animation /*implements IBasicObject, GlobalConstants*/ {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    public float mX, mY, mA;
    public float mVX, mVY, mVA;

    public  int id;
    private int TypeOfObject;
    public int fps;
    protected byte r;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObject (int id, float x, float y, float vx, float vy){
        super (MathUtils.random(0.1f, 0.05f), sDB.BasicPartMap.get(id).textureRegion);
        this.fps = sDB.BasicPartMap.get(id).fps;
        this.mX = x;
        this.mY = y;
        this.mA = 0.0f;
        this.id = id;
        this.mVX = vx;
        this.mVY = vy;
        this.mVA = MathUtils.random( -100.0f, 100.0f);
    }


    public byte Update(float dt){
        //r = 0;
        this.mX += mVX * dt;
        this.mY += mVY * dt;
        this.mA += mVA * dt;

        if (this.mX <= 0 || this.mX >= DisturbingSpace.WIDTH  - this.getKeyFrame(0).getRegionWidth())
            this.mVX *= -1;
        if (this.mY <= 0 || this.mY >= DisturbingSpace.HEIGHT - this.getKeyFrame(0).getRegionHeight())
            this.mVY *= -1;

        return 0;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public int GetTypeOfObject(){
        return this.TypeOfObject;
    }

    public void SetTypeOfObject(int too){
        this.TypeOfObject = too;
    }

    public float GetSpeedX() {  return mVX;   }
    public float GetSpeedY() {  return mVY;   }

    public float GetSpeedA() {
        return mVA;
    }

    public float GetSpeedV() {
        return (float) Math.hypot((double) (-mVX), (double) (-mVY));
    }

    public void SetSpeedXY(float pVX, float pVY) {
        this.mVX = pVX; this.mVY = pVY;
    }

    public void SetSpeedVA(float pVV, float pVA) {
        //          mVX = pVV *(float) (Math.sin(Math.toRadians(this.getRotation())));
        //       mVY = pVV *(float) -(Math.cos(Math.toRadians(this.getRotation())));
        mVA = pVA;
    }

    public void SetSpeedA(float pVA) {
        mVA = pVA;
    }
    public void SetSpeedV(float pVV) {
        this.SetSpeedVA(pVV, this.GetSpeedA());
    }

    // @Override
  //  public float GetCenterX() {
  //      return this.getX() + this.getWidth()/2;
   // }

    // @Override
 //   public float GetCenterY() {
   //     return this.getY() + this.getHeight()/2;
  //  }

    public void PercentModifySpeed(float percent, float dt){
        this.mVX *= (percent - dt)/ percent;
        this.mVY *= (percent - dt)/ percent;
    }

    public int GetObjectID() {
        return this.id;
    }

    // ===========================================================
    // Methods
    // ===========================================================
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}

