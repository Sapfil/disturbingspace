package com.sapfil.ds.World;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.GlobalConstants;
import com.sapfil.ds.Objects.GFXObjects.IBasicObject;
import com.sapfil.ds.Objects.GFXObjects.ZSortedGFXCollecion;
import com.sapfil.ds.Objects.PhysicsObjects.HealthObjectsCollection;
import com.sapfil.ds.XML.DataBaseFromXML;
import com.sapfil.ds.XML.WorldFromXML;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/************************************************
 * Created by Sapfil on 24.01.2016.
 * Prev edit: 02-10-2016
 * Last edit: 20-10-2016 GFX/PHYS separation
 * Notes: Mess-cleaning, Memory-loss check
 ***********************************************/



public class WorldCell /*extends Sprite*/ {

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    public static WorldFromXML sWorldDB;
    public static ZSortedGFXCollecion sGFXheap;

    // ===========================================================
    // Fields
    // ===========================================================

//    private Texture mTechSprite;
    private IBasicObject mTechSprite;
    public HealthObjectsCollection mAsteroidsLayer;

    private int mNodeCenterX, mNodeCenterY; // in ABS coords - needs to checking near or far cell for adding/deleting in world-grid
    private int mHEXCenterX, mHEXCenterY;   // in HEX coords - needs for ... amm... something, maybe parity-check.
    private int TableX, TableY;             // in HEX coords - needs for ... amm... something, maybe parity-check.
    private int IDTableLeftX, IDTableUpperY;// in HEX coords - needs for ... amm... something, maybe parity-check.
    private byte mHEXoffset;                // in HEX coords - needs for parity-check.
    NodeList currentCellObjectsList = null; // obects list from world-data-base

    // ===========================================================
    // Constructors
    // ===========================================================

    WorldCell(int pX, int pY) {
        this(pX, pY, GlobalConstants.QUAD_Direction.CENTER);
    }

    WorldCell(int pX, int pY, GlobalConstants.QUAD_Direction position) {
        super();

        //*** adding technical sprite
        if (GlobalConstants.DEBUG_CELL_TECH_SPRITE) {
//            mTechSprite = new Texture("Grid.png");
//            this.setRegion(mTechSprite);
            mTechSprite = this.sGFXheap.addNewObject(60000, pX, pY, -100);
            mTechSprite.setScale(1, (float)GlobalConstants.CELL_ABS_SIZE_Y / (float)GlobalConstants.CELL_ABS_SIZE_X);
        }

        mAsteroidsLayer = new HealthObjectsCollection();
//        this.setPosition(pX, pY);

        this.mNodeCenterX   = pX;           // in ABS
        this.mNodeCenterY   = pY;           // in ABS
        this.mHEXCenterX    = pX/ GlobalConstants.CELL_ABS_SIZE_X * GlobalConstants.CELL_HEX_SIZE_X; //in HEX
        this.mHEXCenterY    = pY/ GlobalConstants.CELL_ABS_SIZE_Y * GlobalConstants.CELL_HEX_SIZE_Y/2; // in HEX
        this.IDTableLeftX   = mHEXCenterX - GlobalConstants.CELL_HEX_SIZE_X/2;    // in HEX
        this.IDTableUpperY  = mHEXCenterY - GlobalConstants.CELL_HEX_SIZE_Y/2/2;    // in HEX

        //*** if cell data exist in BIG_GREAT_WORLD_DATA_BASE - adding objects to game from database
        //*** if such cell doesnt exist in database - creating empty cell
        //mStaticObjectsTable = new int[GlobalConstants.CELL_HEX_SIZE_X][GlobalConstants.CELL_HEX_SIZE_Y];
        if (sWorldDB.SpaceCellList.get(pX) != null)
            if (sWorldDB.SpaceCellList.get(pX).get(pY) != null) {
                currentCellObjectsList = sWorldDB.cellsList.item(sWorldDB.SpaceCellList.get(pX).get(pY)).getChildNodes();

                for (int j = 0; j < currentCellObjectsList.getLength(); j++)
                    if (currentCellObjectsList.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        // *** reading nex object from DATA_BASE
                        int id = 0; int _x = 0;  int _y = 0;
                        try {id = Integer.parseInt(currentCellObjectsList.item(j).getAttributes().getNamedItem("id").getNodeValue());} catch (Exception ex) {};
                        try {_x = Integer.parseInt(currentCellObjectsList.item(j).getAttributes().getNamedItem("x").getNodeValue());} catch (Exception ex) {};
                        try {_y = Integer.parseInt(currentCellObjectsList.item(j).getAttributes().getNamedItem("y").getNodeValue());} catch (Exception ex) {};

                        // *** calculating IDTable coords and offset
                        TableX = (_x  - this.IDTableLeftX) + 1;
                        TableY = (_y  - this.IDTableUpperY) + 1;
                        if (TableX%2 != 0)   mHEXoffset = 0;
                        else                 mHEXoffset = 1;

                        // *** pasting it to static layer
                        mAsteroidsLayer.addNewObject(id, _x * GlobalConstants.WORLD_GRID_STEP_X, (_y * 2 + mHEXoffset) * GlobalConstants.WORLD_GRID_STEP_Y);
                    }
            }
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int GetCenterX() {
        return this.mNodeCenterX;
    }

    public int GetCenterY() {
        return this.mNodeCenterY;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public void update (float dt){
        mAsteroidsLayer.update(dt);
    }

    public void render(SpriteBatch pSB){
//        if (GlobalConstants.DEBUG_MODE)
//            debugRender(pSB);
        mAsteroidsLayer.render(pSB);
    }

    public void dispose() {
        mAsteroidsLayer.dispose();
        if (GlobalConstants.DEBUG_CELL_TECH_SPRITE)
            sGFXheap.deleteObject(mTechSprite);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

//    private void debugRender(SpriteBatch pSB){
//        pSB.begin();
//        pSB.draw(this.getTexture(), mNodeCenterX - GlobalConstants.CELL_ABS_SIZE_X / 2, mNodeCenterY - GlobalConstants.CELL_ABS_SIZE_Y / 2,
//                GlobalConstants.CELL_ABS_SIZE_X, GlobalConstants.CELL_ABS_SIZE_Y);
//        pSB.end();
//    }

}
