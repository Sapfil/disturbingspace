package com.sapfil.ds.Objects.GFXObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 16-10-2016
 * Notes:
 ***********************************************/
public interface IBasicObject extends Comparable<IBasicObject> {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    //TextureRegion getTextureRegion();

    int getTypeOfObject();
    int getObjectID();
    void setTypeOfObject(int i);

    byte update(float dt);
    void render(SpriteBatch pSB); // needs Begin/end in calling class
    void dispose();               // memory cleaning

    // A is "angle" - rotation in degrees
    // Center is center-point in ABS coords (example: for direct-aiming)
    // Origin is local center for deformation
    float getX();           float getY();           float getA();
    void  setX(float pX);   void  setY(float pY);   void setA(float pA);
    float getCenterX();     float getCenterY();
    float getOriginX();     float getOriginY();
    void setOriginXY(float pX, float pY);

    float getZ();           void setZ(float pZ);  //z-render-coords
    @Deprecated
    int getI();
    @Deprecated
    void setI(int pI);    //number in GFX-heap

    float getSpeedX();      float getSpeedY();
    float getSpeedA();      float getSpeedV();
    void setSpeedXY(float pVX, float pVY);
    void setSpeedVA(float pVV, float pVA);
    void setSpeedA(float pVA);
    void setSpeedV(float pVV);

    void setScale(float pSX, float pSY);

    void percentModifySpeed(float procent, float dt);

    boolean isEnabled();
    void setEnabled(boolean enabled);



    //public boolean isInLevel();
}
