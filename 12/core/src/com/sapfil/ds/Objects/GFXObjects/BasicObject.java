package com.sapfil.ds.Objects.GFXObjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 15.09.2016.
 * Last edit:
 * Notes:
 ***********************************************/

public class BasicObject extends Sprite implements IBasicObject {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    //protected float mX;
    //protected float mY;
    //protected float mA;                // mA is rotaion in degrees
    protected float mZ;
    protected int mI;
    protected  float mVX, mVY, mVA;
    //protected float originX, originY;
    protected float regionWidth, regionHeight;
    protected  int id;
    protected int TypeOfObject;
    protected boolean enabled = true;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicObject(int id) {this (id,0,0);}
    public BasicObject(int id, float x, float y){this (id,x,y, 0,0);}
    public BasicObject(int id, float x, float y, float vx, float vy){
        super (sDB.BasicPartMap.get(id).textureRegion[0]);
        this.regionWidth= sDB.BasicPartMap.get(id).textureRegion[0].getRegionWidth();
        this.regionHeight = sDB.BasicPartMap.get(id).textureRegion[0].getRegionHeight();
        //this.originX = this.regionWidth/2;
        //this.originY = this.regionHeight/2;

        this.setX(x - this.getOriginX());
        this.setY(y - this.getOriginY());

        //this.mX = x - this.originX;
        //this.mY = y - this.originY;
        //TODO try to delete this
        this.setRotation(0.0f);
        this.id = id;
        this.mVX = vx;
        this.mVY = vy;
        this.mVA = 0;

        this.TypeOfObject = sDB.BasicPartMap.get(id).type_of_object;

        this.mVX = vx;
        this.mVY = vy;
        this.mVA = 0;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    //public TextureRegion getTextureRegion() { return super.getTexture(); }

    public int getTypeOfObject(){ return this.TypeOfObject; }
    public int getObjectID() {    return this.id; }
    public void setTypeOfObject(int too){ this.TypeOfObject = too; }

    public float getX() { return super.getX(); }
    public float getY() { return super.getY(); }
    public float getA() { return super.getRotation(); }

    public void setX(float pX) { super.setX(pX); }
    public void setY(float pY) { super.setY(pY); }
    public void setA(float pA) { super.getRotation(); }

    public float getCenterX() { return this.getX() + regionWidth /2; }
    public float getCenterY() { return this.getY() + regionHeight/2; }
    public float getOriginX() { return super.getOriginX(); }
    public float getOriginY() { return super.getOriginY(); }

    public void setOriginXY(float pX, float pY) { super.setOrigin(pX, pY);}

    public float getZ() {return mZ;}
    public void setZ(float pZ) {mZ = pZ;}

    @Deprecated
    public int getI() { return mI; }
    @Deprecated
    public void setI(int pI) {mI = pI; }

    public float getSpeedX() {  return mVX;    }
    public float getSpeedY() {  return mVY;    }
    public float getSpeedA() {  return mVA;    }
    public float getSpeedV() {  return (float) Math.hypot((double) (-mVX), (double) (-mVY));  }

    public void setSpeedXY(float pVX, float pVY) { this.mVX = pVX; this.mVY = pVY; }
    public void setSpeedVA(float pVV, float pVA) {
        mVX = -pVV * (MathUtils.sinDeg(this.getRotation()));
        mVY = pVV  * (MathUtils.cosDeg(this.getRotation()));
        mVA = pVA;
    }

    public void setSpeedA(float pVA) { mVA = pVA; }
    public void setSpeedV(float pVV) { this.setSpeedVA(pVV, this.getSpeedA()); }

    public void percentModifySpeed(float percent, float dt){
        this.mVX *= (percent - dt)/ percent;
        this.mVY *= (percent - dt)/ percent;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setScale(float pSX, float pSY){ super.setScale(pSX, pSY); }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces U/R/D
    // ===========================================================
    public byte update(float dt){
        this.setX(this.getX() + mVX * dt);
        this.setY(this.getY() + mVY * dt);
        this.setRotation(this.getRotation() + mVA * dt);
        return 0;
    }

    // !!!  needs begin/end for pSB in calling class
    public void render (SpriteBatch pSB){
        this.draw(pSB);
    }

    public void dispose(){
    }

    @Override
    public int compareTo(IBasicObject other) {
        if (this.mZ == other.getZ())
            return 0;
        else
        if (this.mZ > other.getZ())
            return 1;
        else return  -1;
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}

