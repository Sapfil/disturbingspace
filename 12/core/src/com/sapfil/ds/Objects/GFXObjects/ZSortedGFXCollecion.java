package com.sapfil.ds.Objects.GFXObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.sapfil.ds.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 20.10.2016.
 * Last edit:
 * Notes:
 ***********************************************/
public class ZSortedGFXCollecion {
    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    protected Array<IBasicObject> objectsArray;
    private boolean isSorted = true;
    private boolean isFull = true;      // no null-objects in list

    // ===========================================================
    // Constructors
    // ===========================================================

    public ZSortedGFXCollecion() {
        Gdx.app.log("Sapfil_Log", "New ZSortedGFXCollecion created");
        objectsArray = new Array<IBasicObject>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getTotalObjects() {
        return (objectsArray.size);
    }

    public IBasicObject getObjectByIndex(int i) {
        return this.objectsArray.get(i);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt) {

            if (!isFull) {
                for (int i = 0; i < objectsArray.size; i++)
                    if (objectsArray.get(i) == null) {
                        objectsArray.removeIndex(i);
                        i--;
                        isSorted = false;
                    }
                isFull = true;
            }


            if(!isSorted) {
                //if (objectsArray.size > 0)
                objectsArray.sort();
                for (int i = 0; i < objectsArray.size; i++)
                    objectsArray.get(i).setI(i);
                isSorted = true;
            }

        return 0;
    }

    public void render(SpriteBatch pSB) {

        if (!isSorted || !isFull)
            this.update(0);

        pSB.begin();
        for (IBasicObject nextSprite : objectsArray)
            if (nextSprite.isEnabled())
                nextSprite.render(pSB);
        pSB.end();
    }

    public void dispose() {
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject(IBasicObject basicObject){
        objectsArray.add(basicObject);
        isSorted = false;
    }

    public IBasicObject addNewObject(Integer id, float x, float y, float z) {
//        objectsArray.sort();

        IBasicObject newObject;
        if (sDB.BasicPartMap.get(id).fps != 0)
            newObject = new BasicAnimatedObject(id, x, y);
        else
            newObject = new BasicObject(id, x, y);
        newObject.setZ(z);
        objectsArray.add(newObject);
        newObject.setI(objectsArray.size - 1);

        isSorted = false;

        return newObject;
    }

    public void deleteObject(int i) {

        this.objectsArray.get(i).dispose();
        objectsArray.set(i, null);
        //isSorted = false;
        isFull = false;
    }

    public void deleteObject(IBasicObject sprite) {

        int index = this.objectsArray.indexOf(sprite, true);
        objectsArray.get(index).dispose();
        objectsArray.set(index, null);
        //isSorted = false;
        isFull = false;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}