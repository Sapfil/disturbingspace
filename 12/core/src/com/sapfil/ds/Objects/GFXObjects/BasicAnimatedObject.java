package com.sapfil.ds.Objects.GFXObjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 15.09.2016.
 * Prev edit: 02-10-2016
 * Last edit: 18-10-16 Incapsulation, mess-cleaning
 * Last edit: 18-02-17 Blasts creation edit
 * Notes: Memory-loss check
 ***********************************************/

public class BasicAnimatedObject extends Animation implements IBasicObject {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    protected float mX;
    protected float mY;
    protected float mZ;
    protected int mI;
    protected float mA;                // mA is rotaion in degrees
    protected float mVX, mVY, mVA;
    protected float originX, originY;
    protected float mSX = 1.0f, mSY = 1.0f;
    protected float regionWidth, regionHeight;
    protected int id;
    protected int TypeOfObject;

    protected float lifeTime = 0.0f;
    protected int fps;
    protected byte r;
    private boolean enabled = true;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasicAnimatedObject (int id){ this(id,0,0);}
    public BasicAnimatedObject(int id, float x, float y){  this(id, x, y, 0, 0);    }
    public BasicAnimatedObject(int id, float x, float y, float vx, float vy){
        super (sDB.BasicPartMap.get(id).duration, sDB.BasicPartMap.get(id).textureRegion);
        this.fps = sDB.BasicPartMap.get(id).fps;

        this.regionWidth= sDB.BasicPartMap.get(id).textureRegion[0].getRegionWidth();
        this.regionHeight = sDB.BasicPartMap.get(id).textureRegion[0].getRegionHeight();
        this.originX = this.regionWidth/2;
        this.originY = this.regionHeight/2;
        this.mX = x - this.originX;
        this.mY = y - this.originY;
        this.mA = 0.0f;                     // mA is rotaion in degrees
        this.id = id;
        this.mVX = vx;
        this.mVY = vy;
        this.mVA = 0;

        this.setPlayMode(PlayMode.REVERSED);
        if ( sDB.BasicPartMap.get(id).fps != 0){
            this.setPlayMode(PlayMode.LOOP);
            if (id >= 900 ){            //blast
                this.setPlayMode(PlayMode.NORMAL);
                this.mA = (float) (Math.random()*360);
            }
            if ( sDB.BasicPartMap.get(id).fps < 0 )
                this.setFrameDuration(this.getAnimationDuration() * MathUtils.random(0.75f, 1.5f));
        }
        this.TypeOfObject = sDB.BasicPartMap.get(id).type_of_object;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    //public TextureRegion getTextureRegion() { return this.getKeyFrame(this.lifeTime); }

    public int getTypeOfObject(){ return this.TypeOfObject; }
    public int getObjectID() {    return this.id; }
    public void setTypeOfObject(int too){ this.TypeOfObject = too; }

    public float getX() { return mX; }
    public float getY() { return mY; }
    public float getA() { return mA; }

    public void setX(float pX) { mX = pX; }
    public void setY(float pY) { mY = pY; }
    public void setA(float pA) { mA = pA; }

    public float getCenterX() { return mX + originX; }
    public float getCenterY() { return mY + originY; }
    public float getOriginX() { return originX; }
    public float getOriginY() { return originY; }
    public void setOriginXY(float pX, float pY) {originX = pX; originY = pY;}

    public float getZ() {       return mZ;    }
    public void setZ(float pZ) {mZ = pZ;      }

    @Deprecated
    public int getI() { return mI; }
    @Deprecated
    public void setI(int pI) {mI = pI; }

    public float getSpeedX() {  return mVX;    }
    public float getSpeedY() {  return mVY;    }
    public float getSpeedA() {  return mVA;    }
    public float getSpeedV() {  return (float) Math.hypot((double) (-mVX), (double) (-mVY));  }

    public void setSpeedXY(float pVX, float pVY) { this.mVX = pVX; this.mVY = pVY; }
    public void setSpeedVA(float pVV, float pVA) {
        mVX = -pVV * MathUtils.sinDeg(mA);
        mVY = pVV  * MathUtils.cosDeg(mA);
        mVA = pVA;
    }

    public void setSpeedA(float pVA) { mVA = pVA; }
    public void setSpeedV(float pVV) { this.setSpeedVA(pVV, this.getSpeedA()); }

    public void percentModifySpeed(float percent, float dt){
        this.mVX *= (percent - dt)/ percent;
        this.mVY *= (percent - dt)/ percent;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setScale(float pSX, float pSY){ mSX = pSX; mSY = pSY; }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces U/R/D
    // ===========================================================

    public byte update(float dt){

        this.mX += mVX * dt;
        this.mY += mVY * dt;
        this.mA += mVA * dt;
        lifeTime += dt;
        if (this.getPlayMode() == PlayMode.NORMAL && this.isAnimationFinished(lifeTime))
            return 1;
        return 0;
    }

    // !!!  needs begin/end for pSB in calling class
    // TODO use local fields for region-H/W or delete local fields
    public void render (SpriteBatch pSB){
        pSB.draw(this.getKeyFrame(this.lifeTime),                 //texture region
                mX, mY,                                           //down-left-corner
                originX, originY,                                 //deform-center
                this.getKeyFrame(this.lifeTime).getRegionWidth(),
                this.getKeyFrame(this.lifeTime).getRegionHeight(),
                mSX, mSY,                                         //stretching deform (default = 1)
                mA);                                              //rotation deform
    }

    public void dispose(){
    }

    public int compareTo(IBasicObject other) {
        if (this.mZ == other.getZ())
            return 0;
        else
            if (this.mZ > other.getZ())
                return 1;
            else return  -1;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}

