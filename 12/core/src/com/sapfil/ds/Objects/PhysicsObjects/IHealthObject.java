package com.sapfil.ds.Objects.PhysicsObjects;

import com.sapfil.ds.Objects.GFXObjects.IBasicObject;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 16-10-2016
 * Notes:
 ***********************************************/
public interface IHealthObject{
        // ===========================================================
        // Constants
        // ===========================================================
        // ===========================================================
        // Getter & Setter
        // ===========================================================

        Body mBody = null;

        float getHealth();
        int getTypeOfBlast();
        Body getBody();

        // ===========================================================
        // Methods
        // ===========================================================

        void resetBody();
        void reduceHealth(float healthToReduce1);
        void kill();
        IBasicObject getSprite();
}
