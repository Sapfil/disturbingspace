package com.sapfil.ds.Objects.PhysicsObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.Objects.GFXObjects.BasicAnimatedObject;
import com.sapfil.ds.Objects.GFXObjects.BasicObject;
import com.sapfil.ds.Objects.GFXObjects.IBasicObject;
import com.sapfil.ds.Objects.GFXObjects.ZSortedGFXCollecion;
import com.sapfil.ds.XML.DataBaseFromXML;

import org.w3c.dom.Node;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Prev edit: 09-10-2016
 * Last edit: 20-10-2016 GFX/PHYS separation
 * Last edit: 18-02-2017 Self creating blasts
 * Notes:
 ***********************************************/
public class HealthObject /*extends BasicAnimatedObject*/ implements IHealthObject, IBasicObject {
    // ===========================================================
    // Constants
    // ===========================================================

    public static DataBaseFromXML sDB;
    public static ZSortedGFXCollecion sGFXHeap;

    // ===========================================================
    // Fields
    // ===========================================================

    private float deltaX, deltaY; // body offset from sprite corner-coordinate
    protected float health;
    private Body mBody;
    public Node worldDBNode = null;
    IBasicObject sprite;
    private byte r;

    // ===========================================================
    // Constructors
    // ===========================================================

    // body_x and body_y *** 02-03-2013
    public HealthObject(int id){this(id,0,0);}
    public HealthObject(int id, float x, float y){this(id,x,y,0,0);}
    public HealthObject(int id, float x, float y, float vx, float vy){this(id, 1, x, y, vx, vy);}
    public HealthObject(int id, float scale, float x, float y, float vx, float vy)	{this(id, scale, 0, x, y, vx, vy, 0);}
    public HealthObject(int id, float scale, float angle,  float x, float y, float vx, float vy, float va){

        if (sDB.BasicPartMap.get(id).fps != 0)
            sprite = sGFXHeap.addNewObject(id, x, y, 0);
        else
            sprite = sGFXHeap.addNewObject(id, x, y, -1);

        //if (angle != 0)
        //    this.setA(angle);

        if (sDB.HealthPartMap.get(id).body_par1 != 0 ){
            if (sDB.HealthPartMap.get(id).bodyType == 0) {      //bodyType == 0 - circle body
                this.mBody = new Body(x, y, sDB.HealthPartMap.get(id).body_par1);
                this.deltaX = sprite.getOriginX();
                this.deltaY = sprite.getOriginY();
            }
            this.health = sDB.HealthPartMap.get(id).health;
            //if (GlobalConstants.DEBUG_MODE)
            //    this.setScale(this.mBody.getWidthOrRadius()*2/this.getTextureRegion().getWidth());
        }
        else this.mBody = null;
    };

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public float getHealth(){
        return health;
    }

    //public void SetNode (Node pNode) {this.worldDBNode = pNode;}
   //public Node GetNode() {return this.worldDBNode;}

    //@Override
    public int getTypeOfBlast() {
        return sDB.HealthPartMap.get(sprite.getObjectID()).type_of_blast;
    }

    public float getX() { return sprite.getX();}
    public float getY() { return sprite.getY();}
    public float getA() { return sprite.getA();}
    public void setX(float pX) {sprite.setX(pX);}
    public void setY(float pY) {sprite.setY(pY);}
    public void setA(float pA) {sprite.setA(pA);}
    public float getCenterX() {return sprite.getCenterX();}
    public float getCenterY() {return sprite.getCenterY();}
    public float getOriginX() {return sprite.getOriginX();}
    public float getOriginY() {return sprite.getOriginY();}
    public void setOriginXY(float pX, float pY) {sprite.setOriginXY(pX, pY); }
    public float getZ() { return sprite.getZ();   }
    public void setZ(float pZ) { sprite.setZ(pZ); }
    public int getI() { return sprite.getI();}
    public void setI(int pI) {sprite.setI(pI);   }
    public float getSpeedX() {return sprite.getSpeedX();}
    public float getSpeedY() {return sprite.getSpeedY();}
    public float getSpeedA() {return sprite.getSpeedA();}
    public float getSpeedV() {return sprite.getSpeedV();}
    public void setSpeedXY(float pVX, float pVY) {sprite.setSpeedXY(pVX,pVY);}
    public void setSpeedVA(float pVV, float pVA) {sprite.setSpeedVA(pVV, pVA);}
    public void setSpeedA(float pVA) { sprite.setSpeedA(pVA);}
    public void setSpeedV(float pVV) { sprite.setSpeedV(pVV);}
    public void percentModifySpeed(float procent, float dt) {sprite.percentModifySpeed(procent, dt);}
    public boolean isEnabled() {return sprite.isEnabled();}
    public void setEnabled(boolean enabled) { sprite.setEnabled(enabled);}
    public void setScale (float pSX, float pSY) {sprite.setScale(pSX, pSY);}

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public int getTypeOfObject() { return sprite.getTypeOfObject();  }
    public int getObjectID()                { return sprite.getObjectID();      }
    public void setTypeOfObject(int i)      { sprite.setTypeOfObject(i);        }

    public byte update(float dt){
        //r = 0;
        r = sprite.update(dt);

        if (mBody != null)
            resetBody();

        if (this.getTypeOfObject() < 0) //dead object
            if (r == 1) // if blast animation finished
                r = 3;

        if (this.health <= 0 && this.getTypeOfObject() > 0){
            this.kill();	// death
            r = 2;
        }
        return r;
    }

    public void render(SpriteBatch pSB) {  sprite.render(pSB);  }

    public void dispose() {
//        sGFXHeap.deleteObject(sprite.getI()); //oldStyle
        sGFXHeap.deleteObject(sprite);
        //sprite.dispose();}
    }

    public void reduceHealth(float healthToReduce1) {
        health -= healthToReduce1;
    };

    public void kill(){
        health = 0;

        sGFXHeap.deleteObject(sprite);
        sprite = sGFXHeap.addNewObject(this.getTypeOfBlast(), this.getCenterX(), this.getCenterY(), -10);

        this.mBody = null;
        this.setTypeOfObject(-this.getTypeOfObject());
    }

    public IBasicObject getSprite() {return sprite;}

    public Body getBody() {
        return this.mBody;
    };

    // ===========================================================
    // Methods
    // ===========================================================

    public void resetBody(){
        this.mBody.setX(this.getX() + deltaX);
        this.mBody.setY(this.getY() + deltaY);
    }

    @Override
    public int compareTo(IBasicObject o) {
        return 0;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
