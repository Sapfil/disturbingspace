package com.sapfil.ds.Objects.PhysicsObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.ds.Objects.GFXObjects.BasicAnimatedObject;
import com.sapfil.ds.Objects.GFXObjects.IBasicObject;
import com.sapfil.ds.Objects.GFXObjects.ZSortedGFXCollecion;

/************************************************
 * Created by Sapfil on 12.09.2016.
 * Prev edit: 16-10-2016
 * Last edit: 18-10-2016 Refactor
 * Notes:
 ***********************************************/
public class HeroMiningGun{

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static ZSortedGFXCollecion sGFXHeap;

    private final int BIAS_X = 23;            //energy beam start bias - from middle of hero sprite
    private final int BIAS_Y = 19;

    // distance from middle of hero sprite to beam-start-point
    private final float BIAS_ABS = (float)Math.sqrt(BIAS_X*BIAS_X + BIAS_Y*BIAS_Y);

    //basic angle between Y-axis of Hero (his middle-line) and BIAS_ABS-line
    private final float BIAS_A = MathUtils.atan2(BIAS_X, BIAS_Y) * MathUtils.radiansToDegrees;

    private final int RAY_RENDER_BIAS_X = 8;   //from low-left corner of sprite to beam-start-point
    private final int RAY_RENDER_BIAS_Y = 3;

    // ===========================================================
    // Fields
    // ===========================================================

    private IBasicObject leftBeam;
    private IBasicObject rightBeam;
    private IBasicObject crossBeam;
    private float angleLeft, angleRight;
    boolean isOn = false;

    // ===========================================================
    // Constructors
    // ===========================================================

    public HeroMiningGun()  {
//
//         = new BasicAnimatedObject(23);
//        rightBeam = new BasicAnimatedObject(24);
//        crossBeam = new BasicAnimatedObject(25);

        leftBeam  = sGFXHeap.addNewObject(23, 0, 0 , 3);
        rightBeam = sGFXHeap.addNewObject(24, 0, 0 , 3);
        crossBeam = sGFXHeap.addNewObject(25, 0, 0 , 3);

        leftBeam.setOriginXY(RAY_RENDER_BIAS_X, RAY_RENDER_BIAS_Y);
        rightBeam.setOriginXY(RAY_RENDER_BIAS_X, RAY_RENDER_BIAS_Y);

        this.Off();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public void setAimPoint (float pX, float pY)    {
            crossBeam.setX(pX - crossBeam.getOriginX());
            crossBeam.setY(pY - crossBeam.getOriginY());
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt){
        leftBeam.update(dt);
        rightBeam.update(dt);
        crossBeam.update(dt);
        return 0;
    }

    public void render (SpriteBatch pSB){
//        if (this.isOn) {
//            pSB.begin();
//
//            leftBeam.render(pSB);
//            rightBeam.render(pSB);
//            crossBeam.render(pSB);
//
//            pSB.end();
//        }
    }

    public void dispose(){
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void On(){
//        isOn = true;
        leftBeam.setEnabled(true);
        rightBeam.setEnabled(true);
        crossBeam.setEnabled(true);
    }

    public void Off() {

//        isOn = false;
        leftBeam.setEnabled(false);
        rightBeam.setEnabled(false);
        crossBeam.setEnabled(false);
    }

    public void setParentParameters(float pX, float pY, float pA) {
        leftBeam.setX(-RAY_RENDER_BIAS_X +  pX + (BIAS_ABS * MathUtils.sinDeg(-pA - BIAS_A)));
        leftBeam.setY(-RAY_RENDER_BIAS_Y +  pY + (BIAS_ABS * MathUtils.cosDeg(-pA - BIAS_A)));
        leftBeam.setA(pA + angleLeft);
        rightBeam.setX(-RAY_RENDER_BIAS_X + pX + (BIAS_ABS * MathUtils.sinDeg(-pA + BIAS_A)));
        rightBeam.setY(-RAY_RENDER_BIAS_Y + pY + (BIAS_ABS * MathUtils.cosDeg(-pA + BIAS_A)));
        rightBeam.setA(pA + angleRight);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public void SetTargetParameters(float pRotaion, float pTargetDiastance, float pTargetAngle){
        float mTargetD = pTargetDiastance - BIAS_Y;

        float BIAS_AIM    =  MathUtils.sinDeg(pTargetAngle - pRotaion) * mTargetD;

        float Distance0 = (float)Math.sqrt(mTargetD * mTargetD + (BIAS_X - BIAS_AIM) * (BIAS_X - BIAS_AIM));
        angleLeft       = -MathUtils.atan2((BIAS_X -  BIAS_AIM), mTargetD) * MathUtils.radiansToDegrees;
        float Distance1 = (float)Math.sqrt(mTargetD * mTargetD + (BIAS_X + BIAS_AIM) * (BIAS_X + BIAS_AIM));
        angleRight      = MathUtils.atan2((BIAS_X + BIAS_AIM), mTargetD) * MathUtils.radiansToDegrees;

        leftBeam.setScale(1, Distance0/61);  //TODO destroy 61!!!
        rightBeam.setScale(1, Distance1/61);
    }
}
