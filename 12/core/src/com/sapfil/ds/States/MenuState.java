package com.sapfil.ds.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.ds.DisturbingSpace;

/************************************************
 * Created by Sapfil on 16.09.2016.
 * Last edit: 02-10-2016
 * Notes:   Memory-loss check
 ***********************************************/
public class MenuState extends State {

    private Texture mBackTexture, mButtonTexture;


    public MenuState(GameStateManager pGSM) {
        super(pGSM);

        mBackTexture    = new Texture("StartScreen.png");
        mButtonTexture  = new Texture("Button.png");
    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.justTouched()){
            gsm.set(new PlayState(gsm));
        }
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch pSB) {

        camera.update();
        pSB.setProjectionMatrix(camera.combined);

        pSB.begin();

        pSB.draw(mBackTexture, 0, 0, DisturbingSpace.WIDTH, DisturbingSpace.HEIGHT);
        pSB.draw(mButtonTexture, DisturbingSpace.WIDTH/2 - mButtonTexture.getWidth()/2, 130);

        pSB.end();

    }

    @Override
    public void dispose() {
        mButtonTexture.dispose();
        mBackTexture.dispose();
    }
}
