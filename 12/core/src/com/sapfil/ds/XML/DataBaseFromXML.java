package com.sapfil.ds.XML;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;

/************************************************
 * Created by Sapfil on ... 01.2016.
 * Last edit: 02-10-2016
 * Notes: Mess-cleaning
 * TODO Memory-loss! Need asset-manager!!!
 ***********************************************/

public class DataBaseFromXML extends XmlReader
{

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private Texture currentTexture;
    public final HashMap<Integer, BasicPartSample> BasicPartMap     = new HashMap<Integer, BasicPartSample>(); // contains all GFX-data
    public final HashMap<Integer, HealthPartSample>  HealthPartMap	= new HashMap<Integer, HealthPartSample>();// contains physics data (health, body size and form, type of blas when die

    // ===========================================================
    // Constructors
    // ===========================================================

    public DataBaseFromXML()	throws Exception {

        FileHandle inputFile = Gdx.files.internal("dataBase.xml");
        //FileHandle inputFile = Gdx.files.internal("XML/dataBase.xml");
        if (!inputFile.file().exists())
            Gdx.app.log("Sapfil_Log", "ERROR in loading DataBase.");
        Element root = this.parse(inputFile);

        for (int i = 0; i < root.getChildCount(); i++) {
            String chapterName = root.getChild(i).getName();
            currentTexture = new Texture(chapterName + ".png");
            for (int j = 0; j < root.getChild(i).getChildCount(); j++) {

                int id              = root.getChild(i).getChild(j).getIntAttribute("id", 0);
                int too             = root.getChild(i).getChild(j).getIntAttribute("too", 0);
                int texture_x       = root.getChild(i).getChild(j).getIntAttribute("x", 0);
                int texture_y       = root.getChild(i).getChild(j).getIntAttribute("y", 0);
                int width           = root.getChild(i).getChild(j).getIntAttribute("w", 0);
                int height          = root.getChild(i).getChild(j).getIntAttribute("h", 0);
                int fps             = root.getChild(i).getChild(j).getIntAttribute("fps", 0);
                int framesInRow     = root.getChild(i).getChild(j).getIntAttribute("fw", 1);
                int framesInColomn  = root.getChild(i).getChild(j).getIntAttribute("fh", 1);

                BasicPartMap.put((Integer) id, new BasicPartSample(id, too,
                        fps, framesInRow, framesInColomn,
                        texture_x, texture_y, width, height, currentTexture));

                // health sample creates only if object have health != 0
                int health = root.getChild(i).getChild(j).getIntAttribute("health", 0);
                if (health != 0){
                    int bodyType    = root.getChild(i).getChild(j).getIntAttribute("bt", 0);
                    int body_width  = root.getChild(i).getChild(j).getIntAttribute("bw", 0);
                    int body_height = root.getChild(i).getChild(j).getIntAttribute("bt", 0);
                    int tob         = root.getChild(i).getChild(j).getIntAttribute("tob", 0);
                    int body_x      = root.getChild(i).getChild(j).getIntAttribute("bx", 0);
                    int body_y      = root.getChild(i).getChild(j).getIntAttribute("by", 0);

                    HealthPartMap.put((Integer)id, new HealthPartSample(id,	bodyType, body_x, body_y,	body_width, body_height,
                            health,	tob));
                }
            }
        }
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================
    // ===========================================================
    // Methods
    // ===========================================================
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    // my class for saving object-samples-information
    public class BasicPartSample{
        public int id, type_of_object;
        public TextureRegion[] textureRegion;
        public int fps;
        public float duration = 0;

        BasicPartSample (int _id, int _too,
                         int _fps, int _framesInRow,  int _framesInColomn,
                         int _texture_x, int _texture_y, int _width, int _height, 	Texture _currentTexture)	{
            this.type_of_object = _too;
            this.id = _id;
            this.fps = _fps;
            if (fps !=0)
                duration = 1/(float)fps;
            TextureRegion tmpTextureRegion = new TextureRegion(_currentTexture, _texture_x, _texture_y, _width, _height);
            TextureRegion[][] tmpRegiosMassive = tmpTextureRegion.split((int)(_width/_framesInRow), (int)(_height/_framesInColomn));
            textureRegion = new TextureRegion[_framesInRow * _framesInColomn];

            for (int i = 0; i < _framesInRow; i++)
                for (int j = 0; j < _framesInColomn; j++)
                    textureRegion[i + j * _framesInRow] = tmpRegiosMassive[j][i];
        }
    }

    public class HealthPartSample {

        public int id;
        public int bodyType, body_x, body_y, body_par1, body_par2;
        public int health;
        public int type_of_blast;
        HealthPartSample(int _id, int bodyType, int _body_x, int _body_y,
                         int _body_width, int _body_height, int _health, int _tob){
            this.bodyType = bodyType;
            this.id = _id;
            this.type_of_blast = _tob;
            this.body_x = _body_x; this.body_y = _body_y;
            this.body_par1 = _body_width; this.body_par2 = _body_height;
            this.health = _health;
        }
    }
}


